## Frequently Asked Questions

### 1. Can I sell KAPEX after the Crowdloan?

**Yes, provided the team wins a Parachain Slot Auction** 

The KAPEX Parachain is in a type of limbo state until it wins a parachain slot on Polkadot. When that happens the network starts to produce blocks and the chain comes to life. However there are a number of steps that need to happen before you can sell your KAPEX:

* 1. The network exists as a shell network, and will be gradulally upgraded following the first blocks being produced. Balance transfers in this network will be limited until it is fully operational.

* 2. Once all the upgrades are completed, the KAPEX allocations will be made based on the Crowdloan contributions by the admin user.

* 3. The Pledge Module will be added to the KAPEX network.

* 4. Balance transfers will be enabled.

**_At this point you will be able to transfer your KAPEX to any exchange that is listing the coins to sell them._**

### 2. Can I get free tokens?

**Yes.** 

A limited supply of free tokens is available from our Totem Meccano Network Faucet **before the Crowdloan starts**. There are several ways to obtain them:

**Sign up!** 

Simply signing up is the easiest way to obtain free tokens.

**Tweet & follow Totem on Twitter.** 

Follow the rewards process [in the App](https://totem.live) 

**Refer a friend.** 

Using your referral link you can invite friends to sign up. If they do your friend will automatically receive the standard sign-up allocation plus a small bonus, and you will receive a bonus as well.

<!-- **Using Totem**

Totem is designed to be used. If you use Totem for tasks, activities and timekeeping as well as payments, your account will receive an additional bonus allocation of TOTEM on MainNet. The more you use Totem the greater the allocation on MainNet. Remember the Developers will not be holding TOTEM and the idea is that the majority of TOTEM must be distributed to the community. So get to work! -->

### 3. What happens to funds allocated from the faucet?

If you receive funds from the Faucet an allocation will be made for you on the Kapex Parachain from the Airdrop reserve. 

### 4. Are HODLers on the Meccano Testnet penalised?

**No.**

### 5. Is this an Initial Parachain Offering?

**Kinda.**

Honestly we are not sure what that means. We are offering KAPEX coins for supporting our Crowdloan, so if you want to call it that, who are we to argue? 

### 6. When I contribute how many confirmations do I have to wait for?

A block is considered "a confirmation" but because of the way the Polkadot network arrives at consensus you should allow around 6 blocks. 

In practice your funds should be accepted by the Crowdloan mechanism within 3-6 seconds after you send it. 

### 7. What happens if I pay after the Crowdloan is closed?

You will not be able to contribute once the Crowdloan closes. hHowever, you can increase your pledge by contributing 5 or more DOTs while the crowdloan is open.

Closing can happen in one of the following ways:

1. Closing happens because the Hard Cap in the Crowdloan is reached. (5.68M DOT contributed)

2. The time for the Crowdloan has passed.

3. Totem KAPEX wins a parachain slot.

4. Pledge Hard Cap is reached. (1.775M DOT pledged)

The Pledge mechanism is considered activated when the module is available on the KAPEX parachain.

### 8. When will my total allocations be calculated?

This will happen in three stages:

1. The first stage happens after the parachain slot is won. This is when the contribution allocations, referrals and Soft and Target Cap are calculated. These are then directly allocated to contributor's addresses.

2. Once the Pledge module is activated it will be populated with amounts to be claimed by Pledgers and Crowdlenders taking into consideration Soft and Target Cap bonuses.

3. The final step is that the module will be activated allowing users to claim their Pledge allocations and bonuses. 

### 9. Can I pay with Fiat?

**No.**

For this reason there is no reason to KYC either.

### 10. Is there a maximum contribution?

**Yes**

The maximum contribution is determined by the maximum number of KAPEX that is available for allocation. 

This is known as the Crowdloan Hard Cap. If the Project Team judges that the Crowdloan is over-subscribed, it can at its own discretion increase the quantity of KAPEX to be allocated.

> The Crowdloan Soft Cap is 568K DOT - 56,800 KAPEX*. <br />
> The Crowdloan Target Cap is 1.775M DOT - 177,500 KAPEX*. <br />
> The Crowdloan Hard Cap is 5.68M DOT - 568,000 KAPEX*.<br />

_*not including bonuses which are paid on top of these caps._

If the Hard Cap is not reached all remaining funds will be assigned to the Network Treasury.

### 11. How can we contact you?

We have a dedicated support channel already built into [Totem Live](https://totem.live/?form=chat&userIds=support). You can also reach us through [Discord](https://discord.gg/TXPKJTAGvt) and [Telegram](https://t.me/totemchat).

Ping us and we should respond.

**Please note:** We are a small team, and we do sleep from time-to-time but you will receive a response. Keep checking back.

### 12. What the hell is KAPEX? I thought Totem had the TOTEM coin?

**In simple terms, KAPEX is a market priced coin and is native to Totem’s KAPEX Parachain.** It's primary purpose is to provide a distribution mechanism to the community with 55% of all tokens being released in the Crowdloan and Pledge. 

TOTEM is an algorithmic stable coin as described [in the Token Economics section](information/overview-token) of this docmentation portal.

### 12a. I still don't get it.

The justification is more involved, but in summary: 

* At this point in time we do not know how many TOTEM should be available to the MainNet network. 

* We do not want to penalise early adopters by setting an amount that is too large. Instead the parachain native coin KAPEX will help us determine exactly how many coins should be available on Totem MainNet. It will be primarily based on KAPEX’s market capitalisation of at the launch of MainNet.

* KAPEX will help us precisely define the economic model by seeing how costly transactions are using a market price coin and to measure the optimal starting quantity of coins for MainNet. This is the main justification for creating KAPEX as a market coin and not a stable coin.

#### How will the coins be migrated from KAPEX to MainNet?

Holders of KAPEX will be expected to [burn](/information/issuance/the-burn.md) their holding in order to receive/mint their allocation of TOTEM on MainNet. In this way, the market decides the market cap and effectively how many Totem will get minted for MainNet. There will be incentives to burn the coins in order to bootstrap MainNet.

KAPEX holder today will be the liquidity providers to actual users on MainNet in future. User - **which could include vary large corporations** - will purchase TOTEM from these early adopters _not the team or Founders_.

Shortly after launch of MainNet (_and provided it reaches stability thresholds_), the developers will relinquish control in MainNet and Totem will become community owned and governed.

