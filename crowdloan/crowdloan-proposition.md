

## Crowdloan Deal at-a-glance

**NOTE This page has been updated. You can see the original version [here](/crowdloan/crowdloan-proposition-original.md).**

### Contribution Guarantee

> All contributors to the Crowdloan will be guranteed a share of the Hard Cap (568,000 KAPEX) paid pro-rata to their Crowdloan Contribution and fulfilled Pledge if we win without reaching the Hard Cap. This is in addition to their KAPEX allocations (based on actual contributions) and bonuses.

We have introduced this feature because the community was concerned that the current Polkadot Parachain winners had won slots with low quantities of DOT contributed. In our previous version of the offering this would result in a low allocation of total supply to contributors. 

**This now guarantees that _no matter what happens_ ALL CONTRIBUTORS will share the entire Hard Cap allocation.**

### Vesting

> **There will be no vesting period following the Totem Crowdloan** - KAPEX coins will be available to transfer as soon as the balance transfer lock is disabled.
>
> Minimum Crowdloan Participation is **5 DOT.**


### KAPEX Coin Supply & Distribution

|                                                      | Supply % | KAPEX       |
|------------------------------------------------------|---------:|------------:|
| **Total KAPEX Supply**                               | **100%**     | **2,581,818** |
| KAPEX Released for Crowdloan & Pledge                    | 55%      | 1,420,000 |
| Development & Engineering Grants | 20%      | 516,364 |
| Airdrop (User Migration from Meccano Network*)                    | 3%       |  77,455  |
| Polkadot Community                         | 2%       |  51,636  |
| Current Team                                | 10%      | 258,182 |
| Future Team                                | 10%      | 258,182 |

*_see terms of migration_

### The Pledge

|                            |      |
|--------------------------------------|:------------|
| Min Pledge Participation             |  0 DOT |
| Max Pledge Participation             |  Up to 100% of Crowdloan contribution amount |

* When the Soft Cap is reached all Pledge participants will receive 10% bonus based on the amount committed to the Pledge.

* When the Target Cap is reached all Pledge participants will receive a further 10% bonus based on the amount committed to the Pledge.

* There is a Cap on the amount of DOT that can be contributed in the Pledge: 1,775,000 DOT at an exchange rate of 0.32 KAPEX per DOT (not including bonuses which are paid on top). When that threshold is reached the Pledge will stop accepting amounts.

* The Pledge amounts will not be collected until after the Parachain slot is won.

### Crowdloan Referral Bonus

To encourage hitting the Soft and Target Caps all Crowdlenders can refer friends and receive a bonus based on their friend's contribution

|                                       | Referee Bonus | Referer Bonus
|--------------------------------------|------------:|-----------------:|
| Referral paid in KAPEX based on referee contribution          |  5%         | 5% |

|                                       |  | 
|--------------------------------------|------------:|
| Max Allocations in the referral program          |    56,800 KAPEX |

_Users who have been referred, must make a contribuition to the Crowdloan, to activate the referral bonus for themselves and the person that referred them. Referral only extends directly to the referred party and is measured by the referred party’s contribution. In other words the bonus does not extend or accumulate to the initial referer when the referee is themselves a referer._

### Base Conversion Rates

All base conversion rates are now fixed. Previously they were dynamically calculated, which made it hard for contributors to work out their allocations. They are as follows:

* The allocation in the Crowdloan is calculated from the contribution of DOT at the conversion rate of **0.1 KAPEX per DOT**.

* The allocation in the Pledge is calculated from the contribution of DOT at the conversion rate of **0.32 KAPEX per DOT**.

All Bonuses use the respective Base Conversion Rate to calculate the amount based on the percentage of the bonus for either the Crowdloan, Pledge or both.

### Soft Cap & Success Bonus

**If the Soft Cap is reached all users will automatically get a bonus.**

|                                      | DOT Qty     | KAPEX Allocation |
|--------------------------------------|------------:|-----------------:|
| Soft Cap for the Crowdloan           |    568,000   |    56,800          |
| Soft Cap for The Pledge              |    177,500     |    56,800           |
| Max Contributor Bonus Allocations <br />[Soft Cap Reached] |  10%           |  11,360          |

### Target Cap & Success Bonus

**If the Target Cap is reached all users will automatically get a bonus.** *

*_This bonus replaces the Soft Cap Bonus (or else you can think of it as a further 10% bonus on top of the Soft Cap Bonus)._

It is intended that if the Target Cap is reached, participants will still be able to pledge funds up to the maximum permitted amount which is 100% of their Crowdloan contributions until the Target Cap is reached.

|                                      | DOT Qty     | KAPEX Allocation |
|--------------------------------------|------------:|-----------------:|
| Target Cap for the Crowdloan                |   1,775,000  |    177,500    |
| Max Contributor Bonus Allocations <br />[Target Cap Reached]  | 20%            |  149,100  |


### Hard Caps

A Hard Cap has been introduced that will limit the amount of DOT that can be contributed to the Crowdloan and Pledge Offering. It is intended that when the Hard Cap is reached no more contributions can be made to the Crowdloan and Pledge. 

There are no additional bonuses paid for reaching the Hard Cap, but the 20% will continue to apply to all contributions above the Target Caps and below the Hard Cap.

|                                      | DOT Qty     | KAPEX Allocation |
|--------------------------------------|------------:|-----------------:|
| Hard Cap for the Crowdloan           |   5,680,000 |      568,000     |
| Hard Cap for The Pledge              |   1,775,000 |    568,000    |
| Max Contributor Bonus Allocations <br />[Hard Cap Reached]  | 20%  |  227,200  |



### Payouts

**There will be two phases to the payouts:**

#### Phase I.a Initial Payout

This initial distribution will be based upon the total contributions in the Crowdloan as logged by the Polkadot Relaychain Crowdloan module.

This will be carried out after the Kapex chain wins a parachain slot and becomes live. Transactions will be executed to distribute the funds directly to contributor's addresses used to make the contributions.

Crowdloan referral bonuses will not be paid at this stage, because the next phases are required to calculate the full bonus payout to contributors even if they were referred.

For existing qualifying users of the Totem Meccano Network, they will receive their migration allocation in Phase II based on their balance in Meccano and their referral bonuses already received. The conversion ratio will be announced once the number of accounts and amounts to migrate are known. There will also be a cut-off date for awards that can be earned in Meccano.

#### Phase I.b Pledge Contribution

The Pledge contributions will then be collected from all participants. We will announce the mechnaism for this step after the parachain goes live.

#### Phase II Distribution of Bonuses

This is the payout stage for both the Pledge contributions and the final total bonuses earned by contributors.

At this point all the necessary information is available to calculate and assess the Cap achievements, Contributor bonuses and Referral Rewards. 

> **Disclaimer: These pages do not constitute investment advice. You should do your own research, and should not participate in this process if you do not understand or feel uncomfortable about committing to the requirements of this program. We offer no guarantees of return of investment [ROI] as DOT prices may vary along with the price of KAPEX. However all DOT committed to the Crowdloan are fully refunded after either the lease period ends or the Crowdloan period whichever is the longer.**

**_In the following pages we outline how you can participate in the Crowdloan_**