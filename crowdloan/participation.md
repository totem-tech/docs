# Participating in the Crowdloan

### Participating in the **Totem Parachain Crowdloan** is a great opportunity for early adopters and for fans of Polkadot - not only do you get to keep the DOTs you commit but you also get an equivalent value in KAPEX coins from Totem. 

> There is no risk to your funds as the Crowdloan is secured by the Polkadot chain, and your DOT is returned to you if Totem does not win. **It's really that simple and really that safe.**

### Types of Participation 

There are two basic types of participation which will award KAPEX coins, although you can win or earn bonuses depending on various factors including referrals and overall crowdloan achievements.

The two types are:

* Polkadot Crowdloan Participation

* The Pledge Participation.

### What is a pledge?

**A pledge is a non-binding signal of intent.** Simply put, you will be signalling your intent to contribute to funding the team at a later date (tbd). **You will not pay any money now**, but in order to collect the bonus you will of course be expected to make good on your pledge. 

> The Pledge is unique to Totem - but is not mandatory for you to participate in the Crowdloan.

The pledge will be managed by the Treasury of the Project Team with an on-chain module created for the purpose. This will not be available until a parachain slot is won.

> **At no time will you ever be _obliged_ to meet the pledge you made - you are free to change your mind.** <br /> This is the fairest mechanism we can conceive to operate such a bonus scheme.

#### Already a Totem Meccano User?

If you have received funds from our Totem Testnet Faucet on the Meccano Network, you will automatically receive an allocation of KAPEX on the Polkadot Parachain in an Airdrop using the same identity that received funds from the faucet. 

**MAKE SURE YOU HAVE [BACKED UP](https://totem.live?form=backup) YOUR IDENTITY FROM TOTEM MECCANO!**  

* Again, these KAPEX funds are only claimable when Totem secures a parachain slot. We will provide instructions for you to do that once the slot is secure. 

Before this happens we will put Totem Meccano into "Maintenance Mode" to lock the current state of balances. You will be expected to claim your funds but they will never be lost.

> The allocations will be pro-rata based on the quantity of KAPEX allocated to this migration - which is 77,455 KAPEX.

**In the next secion we will explain the details of the offering and how to calculate your rewards for contributing**