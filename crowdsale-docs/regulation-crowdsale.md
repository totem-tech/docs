# Regulation, The Howey Test and Swiss FINMA

---

## Regulations

> The Totem Team are building fundamentally a;snbq;hsdfbq;dfshjbqdsfccounting software. 

As such "accounting software" does not require compliance with any known regulatory directive anywhere in the world and therefore the software is free from regulatory oversight.

In addition, the software employes a global Unit of Account as the underlying "functional currency" in the sense of [FASB 52](https://www.fasb.org/page/PageContent?pageId=/reference-library/superseded-standards/summary-of-statement-no-52.html&bcpath=tff), [IAS 21](https://www.iasplus.com/en/standards/ias/ias21). 

It is a fact that a global accounting software application cannot operate in any meaningful mathematical sense without a functional currency mechanism - it is an intrinsic mechanism of the software itself.

> **The Totem Live Accounting Software and Protocols in its fundamental operation as an accounting software system are not subject to any known regulatory scrutiny in any jurisdiction at the time of writing*.**

_*this may change and we reserve the right to amend this position accordingly._

---

As we are aware that Financial Regulators tend to scrutinise blockchain projects to determine if the native token of a project is a security in this section we detail an analysis of the relevant subject matter.

We explore the so-called **"Howey Test"** and **Know Your Customer (KYC) and Anti-Money Laundering (AML) Compliance** particularly in relation to the Crowdfunding/Pledge and Parachain Slot Auction as well as the historical context of use in the Test Network and as a functional currency for accounting purposes. 

In addition as the Live Accounting Association (Swiss Verein in formation), based in Switzerland is administering the Crowdfunding/Pledge and Parachain Slot Auction and must follow the Swiss Financial Market's Authority Guidance which is outlined below.

---

**_Do not register or contribute to the Crowdfunding/Pledge and Parachain Slot Auction if you do not understand anything that is documented here or in the other Crowdfunding/Pledge and Parachain Slot Auction pages._**

---

## The Howey Test 

The Howey Test which came out of the 1946 Securities and Exchange Commission Court Case  [Howey vs SEC](https://en.wikipedia.org/wiki/SEC_v._W._J._Howey_Co.) has become widely accepted as  being applicable to all cryptocurrency fundraising efforts since 2017. 

The purpose of the Howey Test in the context of cryptocurrency is to determine if the Crowdfunding/Pledge and Parachain Slot Auction constitutes an "investment contract" and is therefore considered "a security" to be registered with the SEC.

The Howey Test is set out in four parts. All four parts need to be passed in order for the distribution of Tokens to be considered an "investment contract" and therefore as a security. If one or more parts fail, then the Token _may not_ be considered a security in most jurisdictions.

If the Token is deemed to be a security (i.e. it passes all four tests), then the sale must register with the Securities and Exchange Commission in the USA, regardless of the country in which the Token sale occurs.

The four parts of the test are broadly:

* Is there an investment of money?
* Is there a so-called "Common Enterprise"?
* Is there an expectation of profits or returns on investment?
* Do returns rely solely on the efforts of others?

In the following sections we detail how these tests apply to this Crowdfunding/Pledge and Parachain Slot Auction. Please read this very carefully, and if you have any questions you may reach out to us on any channel for clarification.


### ANALYSIS 1: Is there an investment of money?

The Totem Live Accounting Test Network (aka Totem Meccano Canary Network) is a public blockchain network that has been live since February 14th 2019, and has been the backbone of all the development work that has taken place before and after it was launched.

All users who have registered on the [Totem Live App](https://totem.live) connect directly with the network and have been allocated Totem Transactions (ticker symbol: TOTEM, previously XTX) - the native cryptocurrency of the network - free-of-charge using our automated distribution Faucet since the outset.

The Totem Live Accounting Blockchain Network does not depend on other blockchains such as Ethereum or Bitcoin - it is completely decentralised and independent.

It was intended that the native cryptocurrency is valued as a Unit of Account for accounting purposes, a so-called "functional currrency" based on a basket of currencies. It is used to determine the integer value to be recorded into the accounting ledgers, and is materially identical to all other functional currency "units of account" used by all other accounting mechanisms (software and traditional hand written accounts) that have ever existed.

Where the functional currency of Totem TestNet and the future MainNet differs is that the Unit of Account can be used to evaluate an amount to be charged as a transaction fee and collected as such. It can also be "consumed" in order to execute actions on the Totem Live Accounting Networks. 

> For example to create Activities, Timekeeping and Tasks and to administer them requires a user to spend the an amount of native cryptocurrency equivalent to the calculated amount of functional currrency in order to process these requests on the blockchain network.

Furthermore all existing users (since launch) have also been able to request, with certain limits, further allocations of the native coin (ticker symbol: TOTEM, previously XTX) from the Faucet without charge. This request has been carried out from within the application itself. These additional allocations have been made at a fixed quantity and have been distributed free-of-charge.

The allocations made to users was conditionally carried over to the Totem Live Accounting Parachain as a reward for early adopters.

The Totem TestNet Coin (ticker symbol: TOTEM, previously XTX) is a cryptocurrency that has never been intended to have a value determined by after-market exchanges.

Totem coins on Mainnet and Testnet are designed to have a value that is calculated deterministically by an automated algorithm as set out in our 2016 Paper and more recently updated. The algorithm determines the value of the cyrptocurrencies based on information available to the Totem Network. 

The concept provides that the value of the Token cannot be manipulated by external parties and allows accounting entries to be made with a Unit of Account of deterministic value rather than a currency of floating value. This primarily enables accounting operations to be recorded with the Unit of Account and not the market price.

It should be noted that as the Totem Live Accounting Network is a permissionless blockchain network, no party can prevent after-market exchanges from listing the native cryptocurrencies of the networks. 

However users of third-party exchanges should be aware that the spot price (or market price) is completely disconnected from the deterministicly calculated price in the Totem Live Accounting Network, and therefore users must consider and are entirely responsible for impairment risks when selling or buying on after-market exchanges.

The value of the cryptocurrency allocated either by the Faucet is in accordance with the valuation algorithm. At the time of the Meccano Test network startup, this value was equivalent to around 0.18 USD at a time of allocation or 16181 TOTEM. 

This amount is sufficient for micro-business to conduct accounting operations and business activities in the Totem Live Accounting Application essentially free-of-charge for a meaningful period of time without requiring an investment of fiat or cryptocurrency funds.

In the case of a request to replenish TOTEM, the Faucet allocated the funds unconditionally and free-of-charge although this has since been deprecated in April 2022.

However on the Totem Kapex Network, the native functional currency is intended to be subject to market price valuations rather than having a deterministic Unit of Account valuation.

These coins will be partially distributed free-of-charge as follows:

* Allocations will be distributed with a to-be-determined ratio from Totem TestNet users to their same address on the Kapex Network.

* Allocations will be made free-of-charge according to the number of DOT committed to secure a lease in a "Polkadot Crowdloan" at a ratio of not less than 0.1 Kapex per DOT contributed. 

    * The Crowdloan commitment is fully refundable after the end of the lease period.


* Proportional to the commitment by users to donate DOT in a pledge to the Team at a ratio of not less than 0.32 Kapex per DOT donated. 

The Crowdfunding/Pledge and Parachain Slot Auction will be administered by the Live Accounting Association (Swiss Verein in formation).

The Live Accounting Association (Swiss Verein in formation) will set aside money received as a donation to enable the purpose of the Association.

> The purpose of the Live Accounting Association (Swiss Verein in formation) is to provide funding for the development of the Live Accounting Network. Any developer - not just the Founders of Totem Accounting - can apply for grants from the Association for work that carries out the purpose of the Association.

* Founders will not receive an allocation of TOTEM on Mainnet, KAPEX on Polkadot Parachain from the Crowdfunding/Pledge and Parachain Slot Auction.
* In future all balances will be transferred to Totem Live Accounting MainNet at a minimum of 1:1 ratio of allocation.

 



#### Howey Test 1 Conclusion 

The first test is unlikely to be satified as Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) were distributed on an already live network via an already live application, free-of-charge and for a significant period of time prior the Crowdfunding/Pledge and Parachain Slot Auction.

Therefore on this point the Token does not qualify as a Security. Consideration must still be given to the other tests.

---

### ANALYSIS 2: Is there a so-called "Common Enterprise"?

As the Totem Live Accounting Network has already been developed and is already live, there is no requirement for the Contributors to the Crowdfunding/Pledge and Parachain Slot Auction to be part of a common enterprise in order for the project to be developed, launched or to succeed.

The TOTEM on Mainnet, KAPEX on Polkadot Parachain will be unlocked for Contributors shortly after the Crowdfunding/Pledge and Parachain Slot Auction completes using the application itself. Contributor can immediately use some or all of their allocations of TOTEM on Mainnet, KAPEX on Polkadot Parachain  to enable the application or to transfer to other parties as a form of settlement.

#### Howey Test 2 Conclusion

The second test fails to establish a common enterprise, as Contributors are participating after the application has been developed and available to use.

---

### ANALYSIS 3. Is there an expectation of profits or returns on investment?

There are three categories of Totem Transaction owners:

* Early users / owners who were allocated TOTEM on Meccano Testnet from the Faucet
* Crowdfunding/Pledge and Parachain Slot Auction Contributor owners who were allocated KAPEX on Polkadot Parachain from the Faucet as a function of their contribution.
* Owners who are both of the above.

For users allocated Totem native cryptocurrency from the Faucet alone there is no expectation of profit. No publicity has ever been made to this effect at any time.

Totem Transactions that are allocated by the Live Accounting Association (Swiss Verein in formation) in the Crowdfunding/Pledge and Parachain Slot Auction are allocated at a discount to the deterministic exchange rate. The discounts are set out in the schedules to the Crowdfunding/Pledge and Parachain Slot Auction and are entirely dependent on the contribution level.

It could be argued that Contributors can expect that the TOTEM on Mainnet, KAPEX on Polkadot Parachain will be accepted by other parties on the Network at the deterministic exchange rate and therefore an element of increase in value is built-in to the discounted distribution from the Crowdfunding/Pledge and Parachain Slot Auction. 

However this is only true if the contributor intends not to use Totem Live Accounting and therefore special conditions exist for so-called "Hodlers". It means that penalties apply under certain circumstances to "Hodlers" as set out in the Crowdfunding/Pledge and Parachain Slot Auction FAQ. Therefore in order for returns to be realised the user must also perform accounting tasks that are useful for them.

It is however entirely expected that when the native cryptocurrency is used for example in conjuction with paying supplier invoices the value of that settlement is agreed based on the deterministic exchange rate algorithm or any other value that the parties agree on. At current rates, and given that the mechanism is designed to provide a stable value, this will mean an increase received in relative value compared to the discounted value of all contributions.

#### Howey Test 3 Conclusion 

If the Howey Test was based on this test alone there would be a possibility that the discount allocation is considered a security. As the exchange rate on Mainnet is designed to be stable and the discounted allocation infers a future usage value all Contributors will have a return value known at the outset.

This is subject however to the correct functioning of the algorithm that determines the value of TOTEM on Mainnet, for exchange. Should any factors significantly change the valuation, Contributors cannot necessarily expect a return until the algorithm has been throughly proven and therefore the expectation of profit is not guaranteed.

This test is therefore only partially satisfied and subject to interpretation.

---

### ANALYSIS 4. Do returns rely solely on the efforts of others?

In traditional so-called Initial Coin Offerings (ICO) the project is just an idea. At the point of the ICO the application has not been developed yet, and may fail if the development team is not able to achieve their goals. In these cases it is clear that investments in ICOs would require others to do work in order to realise a profit or return.

The Totem Accounting development team has been entirely Founder-funded until this point and the project began some time in 2014. There are no external investors and never has been. 

The Totem Accounting development team have developed the blockchain network and the core functionality of the application before beginning a funding round. 

Potential investors are in a vastly different position with Totem Live Accounting as they can make valid judgements about the Team and its abilities based on what they have achieved and delivered to-date. 

The contributions they make are aimed at providing funds to accelerate the development of the remaining aspects of the application and to begin the process of adoption.

In essence Totem Live Accounting, isn't an idea any more. It's no longer even an MVP. It's a long way down the road to becoming a full product. 

Furthermore because the entire development is completely opensource, any party can join the network, build their own applications and User Interfaces without having to rely on the Founding Team.

#### Test 4 Conclusion 

The Founding Team has already completed the work to enable the network. It is no longer dependent on their efforts to function or operate. However, it is dependent on their vision and credibility to accelerate the goal towards widespread adoption.

The network is already functional, the code is open source, and the documentation about the project aims are is in the public domain. 

These facts alone mean that the Howey Test is not satified that the enterprise relies on the efforts of others to continue. 

---

## Howey Test Conclusions

Long before the Crowdfunding/Pledge and Parachain Slot Auction, the Totem Live Accounting Network and Application became public and operational with the core functionality having already been built, launched and available to the public even in the Test Network form.

Furthermore TOTEM on Meccano Testnet, that will also be migrated to the Parachain and eventually the Mainnet has been issued and distributed for a significant period of time prior to the Crowdfunding/Pledge and Parachain Slot Auction free-of-charge and will continue to allocated in this way, albeit in some limited capacity, after the Crowdfunding/Pledge and Parachain Slot Auction completes.

The users of the Network are not required to carry out work in order to give value to the Token as the value is determined by an independent algorithm rather than a third-party market price. 

The application has almost entirely been completed by the Founders prior to the Crowdfunding/Pledge and Parachain Slot Auction and can be used today although there are still many parts to be added.

This analysis is subject to alternative interpretation however for the reasons set out above three out of the four tests fail and on this basis the Totem Transaction Token (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) is unlikely to considered a security in the USA.

This opinion may change in future or should the legislation change.

---

# Swiss FINMA Regulations

Under the Swiss Markets authority Guidance (FINMA Guidance 04/2017), Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) may fall under one or several categories of Token:

* A Payment Token
* A Registered or Unregistered Security Token
* An Asset Token

The categorisation is not mutually exclusive and can be cumulative.

A further categorization is possible, the so-called Pre-Sale or Pre-Financing of a project. In the case of TOTEM which is already in circulation this categorisation is not applicable.

### ANALYSIS 5. Are Totem Transactions Payment Tokens?

Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) are in the classic sense a cryptocurrency. They are intended to be consummed when transactions are executed on the blockchain network, and can be used for payments. The quantity of TOTEM on Mainnet, KAPEX on Polkadot Parachain  that is consumed by a transaction depends on the byte size of the transaction and other technical factors including the type of accounting record to be stored on the blockchain, and the amount of time a transaction will take.

However, unlike traditional cryptocurrencies the value of TOTEM on Mainnet, is deterministic, as it is designed to be used as a Unit of Account within the accounting engine. In this case the value that is recorded may not have any bearing on a specific blockchain transaction, and may not even be related to an asset that is held.

#### Test 5 Conclusion

Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) are defined as a cryptocurrency and given the nature of the value calculation, will inevitably be used as a settlement currency although this mechanism was primarily designed as a Unit of Account for valuation and accounting purposes. 

TOTEM on Mainnet, KAPEX on Polkadot Parachain can be considered a Payment Token and is therefore not treated as a security in this context. 

---

### ANALYSIS 6. Are Totem Transactions Utility Tokens?

By definition (ticker symbol: TOTEM on Mainnet) are Utility Tokens whose purpose is to confer digital access rights to the blockchain network. In the case of the Parachain, the Polkadot cryptocurrency DOT will provide the underlying access to the blockchain network.

Furthermore  Totem Transactions (ticker symbol: TOTEM on Mainnet) they are not intended to be traded as a free-floating asset on capital markets due to the algorithmic mechanism by which the Token value is calculated. This is not the case for KAPEX on the Parachain.

#### Test 6 Conclusion

In this case the Totem Transactions (ticker symbol: TOTEM on Mainnet) exhibit missing properties that would otherwise class it as a security, for example to provide access to capital markets. KAPEX has no guarantees that it will be listed, as it will be part of the Polkadot Network, but will not be the primary cryptocurrency for transactions.

---

### ANALYSIS 7. Are Totem Transactions Asset Tokens?

Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) that are to be distributed in the Crowdfunding/Pledge and Parachain Slot Auction, are allocated on an existing so-called Testnet system. They are intended to be migrated to the MainNet and Parachain when it goes live. 

The allocation is conditional upon the contributor to comply with the terms of migration, but in general remaining balances on accounts will be migrated plus any additional bonuses earned in the interim, and these will need to be claimed by the user.

The TOTEM on Mainnet, KAPEX on Polkadot Parachain Tokens are standardised, however due to the algorithmic mechanism by which the TOTEM Token value is calculated, they are not intended for mass standardised trading.

Furthermore TOTEM's value on Mainnet is derived from a basket of independent assets that are not actually held or used as collateral in this calculation. In other words there are no assets that are held which underwrite the value of Totem Transactions. The value is primarily calculated for accounting purposes in the application itself.

Totem Transactions have been self-issued since April 2019 and although it has not been publicised, given the fact that a Crowdfunding/Pledge and Parachain Slot Auction will occur there may be an expectation of a claim on the MainNet for non-paying owners.

#### Test 7 Conclusion

Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) may be considered uncertificated securities under the Swiss Stock Exchange Act (SESTA). Self-issued uncertificated securities in Switzerland are esentially unregulated even if uncertificated securities qualify as securities within the meaning of the Financial Market Infrastructure Act (FIMIA). 

Totem Transactions (ticker symbol: TOTEM on Mainnet, KAPEX on Polkadot Parachain) are not intended to be traded on the Primary [Stock] Market and therefore do not need to be regulated in accordance with directives in Article 3 paragrah 3 [Stock Exchange Ordinance] of SESTA.

The issuance of Totem Transactions are not analgous to Equity or Bonds and although FINMA has no direct responsibility in this area, the Token is not required to conform to the Swiss Code of Obligations and the Financial Services Act (FINSA).

---

### ANALYSIS 8. Other considerations under Swiss Regulations

#### Deposits

This issuance of Totem Transactions by the Live Accounting Association (Swiss Verein in formation) in exchange for contributions does not constitute a claim for the repayment of funds, and therefore the contributions are not considered deposits. There is no requirement under the Banking Act to require a licence. 

#### Collective Investment Schemes

The funds collected by the Live Accounting Association (Swiss Verein in formation) are not to be managed by third parties and are therefore is not a collective investment scheme.

# KYC and AML Regulations

In Switzerland in the case of utility tokens anti-money laundering regulation is not applicable as long as the main reason for issuing the tokens is to provide access rights to a non-financial application of blockchain technology (see Art. 2 para. 2 let. a no. 3 AMLO, FINMA Circ. 11/1 "Financial intermediation under AMLA" margin no. 13 et seq.).

With respect to the Parachain Slot Auction the contribution is voluntary, random, and carried out directly ion the Polkadot Network and not in the Totem Network. There is no ability to predict or identify participants or indeed to collect their Personal Identification Information (PII).

Furthermore the contributions are not made to the Developers, founders or any member of the team, or the Live Accounting Association (Swiss Verein in formation). Therefore in the case of the Polkadot Parachain Auctions no KYC/AML will be required or collected by any party.

However, regarding the Crowdfunding/Pledge and in the interest of transparency the Live Accounting Association (Swiss Verein in formation) has taken a pragmatic approach in deciding to capture some personal information in order to satisfy requests from third-countries law enforcement agencies and to protect the Association, its directors and members.

In the interests of securing the Personal Identification Information (PII) the following proceedure will be followed: 

* The application found at the domain https://totem.live will manage Contributors' registration.

* The data that is submitted by users will not be accessible by the application servers - with the exception of the pay-to cryptocurrency address that is sent to registrants.

* The details will be encrypted on the client side (i.e. on the registrant's own device) not by our Totem Accounting's servers.

* This encryption is in addition to the encryption that takes place via the secure websocket using the Secure Websocket Protocol `wss://` and a valid CA certificate. 

    * In this way only the Live Accounting Association (Swiss Verein in formation)'s public encryption key is used for encryption and therefore Totem Accounting or any thrid party cannot view or decrypt the registrant's personal data.

* This also means that registrant's PII can be transmitted securely over the Internet and stored securely on the Live Accounting database without needing to decrypt it.

* All intermediate servers including the Totem Accounting Messaging Server will not be able to decrypt the information as they will not have access to the decryption keys of the Live Accounting Association (Swiss Verein in formation). 

* All Personal Identification Informtion (PII) will be held in this encrypted format on the Live Accounting Association (Swiss Verein in formation) database. 

* The Live Accounting Association (Swiss Verein in formation) will not decrypt the data other than for verification purposes or following a formal written request made to the Live Accounting Association (Swiss Verein in formation) by law enforcement. 

    * As the Live Accounting Association (Swiss Verein in formation) is not obliged to go through this process all law enforcement requests will be made public by the Live Accounting Association (Swiss Verein in formation) stating which agency, when and for what reason they were requested.

    * The data provided in these requests will only ever be decrypted on a case-by-case basis and not decrypted _en masse_.

All Contributors who do not provide valid PII may be excluded from receiving bonus KAPEX on Polkadot Parachain allocations on the Totem Parachain regardless of their contribution. Supplying false information is therefore at the risk of the registrant.

The personal Information that we request is as follows:

* Given name and Family name

* Residential Address including Postcode and Country
    * Residents of certain countries are excluded from participating in the Crowdfunding/Pledge and Parachain Slot Auction, but ***are not excluded from using the app***. See the [Crowdfunding/Pledge Terms](Crowdfunding.md).

* Email address
    * Registrants' email addresses will be shared by the Live Accounting Association (Swiss Verein in formation) with the Totem Accounting and subscribed to the mailing list in order to facilitate announcements about the Crowdfunding/Pledge and Parachain Slot Auction progress and other news or relevant communications.