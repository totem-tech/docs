# Regulation, The Howey Test and Swiss FINMA

In this section we detail the two most important considerations; the so-called **"Howey Test"** and **Know Your Customer (KYC) and Anti-Money Laundering (AML) Compliance** in relation to the Crowdfunding. 

In addition as the Live Accounting Association (Swiss Verein in formation), based in Switzerland is administering the Crowdfunding and must follow the Swiss Financial Market's Authority Guidance which is outlined below.

---

**_Do not register or contribute to the Crowdfunding if you do not understand anything that is documented here or in the other Crowdfunding pages._**

---

## The Howey Test 

The Howey Test which came out of the 1946 Securities and Exchange Commission Court Case  [Howey vs SEC](https://en.wikipedia.org/wiki/SEC_v._W._J._Howey_Co.) has become widely accepted as  being applicable to all cryptocurrency fundraising efforts since 2017. 

The purpose of the Howey Test is to determine if the Crowdfunding constitutes an "investment contract".

The Howey Test is set out in four parts. All four parts need to be passed in order for the distribution of Tokens to be considered an "investment contract" and therefore as a security. If one or more parts fail, then the Token _may not_ be considered a security in most jurisdictions.

If the Token is deemed to be a security (i.e. it passes all four tests), then the sale must register with the Securities and Exchange Commission in the USA, regardless of the country in which the Token sale occurs.

The four parts of the test are broadly:

* Is there an investment of money?
* Is there a so-called "Common Enterprise"?
* Is there an expectation of profits or returns on investment?
* Do returns rely solely on the efforts of others?

In the following sections we detail how these tests apply to this Crowdfunding. Please read this very carefully, and if you have any questions you may reach out to us on any channel for clarification.


### ANALYSIS 1: Is there an investment of money?

The Totem Live Accounting Test Network (aka Totem Meccano Canary Network) is a public blockchain network that has been live since February 14th 2019, and has been the backbone of all the development work that has taken place before and after it was launched.

All users who have registered on the [Totem Live App](https://totem.live) connect directly with the network and have been allocated Totem Transactions (ticker symbol: TOTEM) - the native cryptocurrency of the network free-of-charge using our automated Faucet since the outset.

The Totem Live Accounting Blockchain Network does not depend on other blockchains such as Ethereum or Bitcoin - it is completely decentralised and independent, and therefore the native token is considered a native coin rather than a token.

TOTEM is consumed in order to execute actions on the Totem Live Accounting Network. For example to create Activities, Timekeeping and Tasks and to administer them requires you to spend TOTEM to process these requests on the blockchain network.

Furthermore all existing users since launch have also been able to request, with certain limits, further allocations of Transactions (ticker symbol: TOTEM) from the Faucet without charge. This request is carried out from within the application itself. These additional allocations have been fixed at the same level as the initial allocation and have been distributed free-of-charge.

The allocations made to users will be unconditionally carried over to the Totem Live Accounting Parachain Network as a reward for early adopters following the completion of certain tasks in the application connected to the Meccano Test Network.

The Totem Transactions Token (ticker symbol: TOTEM) is a cryptocurrency that has never been intended to have a value determined by after-market exchanges. 

TOTEM has been designed from the outset to represent a unit of measure calculated deterministically by an automated algorithm as set out in our 2016 Paper and more recently updated. The algorithm determines the conversion of the TOTEM unit of measure based on information available tothe Totem Network. 

The concept provides that the value of the unit of measure cannot be manipulated by external parties and allows accounting entries to be made with a deterministic value rather than a currency of floating value. This primarily enables accounting operations to be recorded in the unit of measure for accounting purposes more widely understood as a Unit of Account. The coin is therefore not a market priced coin.

It should be noted that as the Totem Live Accounting Network is a permissionless blockchain network, no party can prevent after-market exchanges from listing TOTEM should they decide to do so, and the team behind the development cannot be implicated in that event. 

However, users of third-party exchanges should be aware that the spot price (or market price) is completely disconnected from the deterministicly calculated price in the Totem Live Accounting Network, and therefore users must consider and are entirely responsible for impairment risks when selling or buying on after-market exchanges.

The conversion of the TOTEM coin allocated either by the Faucet or the Crowdfunding is in accordance with the determination algorithm. At the time of the network start, this conversion rate was equivalent to around 0.18 USD at a time of allocation or 16181 TOTEM.

This amount is sufficient for micro-business to conduct accounting operations and business activities in the Totem Live Accounting Application essentially for free for a meaningful period of time without requiring an investment of fiat or cryptocurrency funds.

##### Kapex Network and Coins

The Totem Meccano Network was the initial development environment for the Kapex Parachain Network which is currently live and the Totem Mainnet which has yet to launch. The development will migrate to these other networks before the Meccano Network is deprecated.

The first of these is the Kapex Parachain Network. This is technically a decentralised autonomous blockchain network, that is able to communicate with the Polkadot network via a messaging system called XCM. In addition the Polkadot Network can offer the security of its own validators to any Parachain that puts up a sum of DOT (the native cryptocurrency of the Polkadot Network) to secure a lease on that network.

In the case of the Kapex Parachain, the community put up enough DOT to secure a lease slot for the Kapex Parachain. The lease is in fact a guarantee that never leaves the control of the entity that makes the contribution, however it is locked for a paeriod of around 96 weeks.

In exchange for the lost opportunity cost of locking the DOT for the period of the lease, the Totem team offered to give away free-of-charge a quantity of Kapex Coins (ticker symbol: KPX). At the time of writing the final rate of exchange was circa 3.5 Kapex per DOT committed to the lease guarantee including bonuses.

The process by which this took place is called a "Crowdloan" and is executed on the Polkadot Network. The Crowdloan was entirely out of control of the Totem Team. Furthermore, the funds are unable to be used or transferred by the Team or transferred from the Crowdloan Account. The Polkadot Network manages the locking and release of funds automatically without intervention from the Totem Team.

It is intended that the Founders will not receive an allocation of TOTEM from the Crowdloan process, but will receive an allocation in lieu of their efforts to develop the application, blockchain and Live Accounting Network.

It is intended that in future all balances of KPX will be transferred to Totem Live Accounting MainNet at a minimum of 1:1 ratio of allocation. 

#### Howey Test 1 Conclusion 

The first test is unlikely to be satified as Totem Transactions (ticker symbol: TOTEM) were distributed on an already live network via an already live application, free-of-chare for a significant period of time.

Furthermore, the Kapex coin (ticker symbol: KPX) has been given away free-of-charge without any condition of investment. The allocation of Kapex Coins are not equivalent to a non-returnable risk capital investment.

Therefore on this point neither TOTEM nor KPX qualifies as a Security. Consideration must still be given to the other tests.

---

### ANALYSIS 2: Is there a so-called "Common Enterprise"?

As the Totem Live Accounting Network has already been developed and is already live both as the Meccano Testnet and as a Production Network in the Kapex Parachain there is no requirement for the Contributors to the Crowdloan to be part of a common enterprise in order for the project to be developed, launched or to succeed.

#### Howey Test 2 Conclusion

The second test fails to establish a common enterprise, as Contributors are participating after the application has been developed and available to use.

---

### ANALYSIS 3. Is there an expectation of profits or returns on investment?

There are three categories of Totem Transaction owners:

* Early users / owners who were allocated TOTEM from tne Faucet
* Crowdloan Contributors who were allocated KPX as a function of their contribution
* Owners who are both of the above

For users allocated Totem Transactions (ticker symbol: TOTEM) from the Faucet alone there is no expectation of profit. No publicity has ever been made to this effect at any time. However, there is an expectation that these coins can be exchanged for KPX Coins on the Kapex Parachain Network.

KPX that are allocated by the Live Accounting Association (Swiss Verein in formation) in the Crowdloan are allocated free-of-charge but are dependent on the level of contribution in DOT.

It could be argued that Contributors can expect that the KPX allocated to them will be accepted by other parties on the Network at a value determined by the open market and therefore an element of speculative increase in value is built-in to the distribution from the Crowdloan. 

However this is only true if the contributor intends not to use Totem Live Accounting Application and therefore special conditions exist for so-called "Hodlers". It means that penalties apply under certain circumstances to "Hodlers" as set out in the Crowdfunding FAQ. Therefore in order for returns to be realised the user must also perform accounting tasks that are useful for them.

It is however entirely expected that when TOTEM is used for example in conjuction with paying supplier invoices the value of that settlement is agreed based on the deterministic exchange rate algorithm. At current rates, and given that the mechanism is designed to provide a stable value, this will mean an increase received in relative value compared to the discounted value of all contributions.

#### Howey Test 3 Conclusion 

If the Howey Test was based on this test alone there would be a possibility that the discount allocation is considered a security. As the exchange rate is designed to be stable and the discounted allocation infers a future usage value all Contributors will have a return value known at the outset.

This is subject however to the correct functioning of the algorithm that determines the value of TOTEM for exchange. Should any factors significantly change the valuation, Contributors cannot necessarily expect a return until the algorithm has been throughly proven and therefore the expectation of profit is not guaranteed.

This test is therefore only partially satisfied and subject to interpretation.

---

### ANALYSIS 4. Do returns rely solely on the efforts of others?

In traditional so-called Initial Coin Offerings (ICO) the project is just an idea. At the point of the ICO the application has not been developed yet, and may fail if the development team is not able to achieve their goals. In these cases it is clear that investments in ICOs would require others to do work in order to realise a profit or return.

The Totem Accounting development team has been entirely Founder-funded until this point and the project began some time in 2014. There are no external investors and never has been. 

The Totem Accounting development team have developed the blockchain network and the core functionality of the application before beginning a funding round. 

Potential Investors are in a vastly different position with Totem Live Accounting as they can make valid judgements about the Team and its abilities based on what they have achieved and delivered to-date. 

The contributions they make are aimed at providing funds to accelerate the development of the remaining aspects of the application and to begin the process of adoption.

In essence Totem Live Accounting, isn't an idea any more. It's no longer even an MVP. It's a long way down the road to becoming a full product. 

Furthermore because the entire development is completely opensource, any party can join the network, build their own applications and User Interfaces without having to rely on the Founding Team.

#### Test 4 Conclusion 

The Founding Team has already completed the work to enable the network. It is no longer dependent on their efforts to function or operate. However, it is dependent on their vision and credibility to accelerate the goal towards widespread adoption.

The network is already functional, the code is open source, and the documentation about the project aims are is in the public domain. 

These facts alone mean that the Howey Test is not satified that the enterprise relies on the efforts of others to continue. 

---

## Howey Test Conclusions

Long before the Crowdfunding, the Totem Live Accounting Network and Application became public and operational with the core functionality having already been built, launched and available to the public even in the Test Network form.

Furthermore TOTEM has been issued and distributed for a significant period of time prior to the Crowdfunding for free and will continue to allocated in this way, albeit in some limited capacity, after the Crowdfunding completes.

The users of the Network are not required to carry out work in order to give value to the Token as the value is determined by an independent algorithm rather than a third-party market price. 

The application has almost entirely been completed by the Founders prior to the Crowdfunding and can be used today although there are still many parts to be added.

This analysis is subject to alternative interpretation however for the reasons set out above three out of the four tests fail and on this basis the Totem Transaction Token (ticker symbol: TOTEM) is unlikely to considered a security in the USA.

This opinion may change in future or should the legislation change.

---

# Swiss FINMA Regulations

Under the Swiss Markets authority Guidance (FINMA Guidance 04/2017), Totem Transactions (ticker symbol: TOTEM) may fall under one or several categories of Token:

* A Payment Token
* A Registered or Unregistered Security Token
* An Asset Token

The categorisation is not mutually exclusive and can be cumulative.

A further categorization is possible, the so-called Pre-Sale or Pre-Financing of a project. In the case of TOTEM which is already in circulation this categorisation is not applicable.

### ANALYSIS 5. Are Totem Transactions Payment Tokens?

Totem Transactions (ticker symbol: TOTEM) are in the classic sense a cryptocurrency. They are intended to be consummed when transactions are executed on the blockchain network, and can be used for payments. The quantity of TOTEM that is consumed by a transaction depends on the byte size of the transaction and other technical factors including the type of accounting record to be stored on the blockchain.

However, unlike traditional cryptocurrencies the value of TOTEM is deterministic, as it is designed to be used as a Unit of Account within the accounting engine. In this case the value that is recorded may not have any bearing on a specific blockchain transaction, and may not even be related to an asset that is held.

#### Test 5 Conclusion

Totem Transactions (ticker symbol: TOTEM) are defined as a cryptocurrency and given the nature of the value calculation, will inevitably be used as a settlement currency although this mechanism was primarily designed as a Unit of Account for valuation and accounting purposes. 

TOTEM can be considered a Payment Token and is therefore not treated as a security in this context. 

---

### ANALYSIS 6. Are Totem Transactions Utility Tokens?

By definition Totem Transactions are Utility Tokens whose purpose is to confer digital access rights to the blockchain network.

Furthermore they are not intended to be traded as a free-floating asset on capital markets due to the algorithmic mechanism by which the Token value is calculated.

#### Test 6 Conclusion

In this case the Totem Transactions (ticker symbol: TOTEM) exhibit missing properties that would otherwise class it as a security, for example to provide access to capital markets.

---

### ANALYSIS 7. Are Totem Transactions Asset Tokens?

Totem Transactions (ticker symbol: TOTEM) that are to be distributed in the Crowdfunding, are allocated on an existing so-called Testnet system. They are intended to be migrated to the MainNet system when it goes live. 

The allocation is conditional upon the contributor to comply with the terms of migration, but in general remaining balances on accounts will be migrated plus any additional bonuses earned in the interim.

The TOTEM Tokens are standardised, however due to the algorithmic mechanism by which the Token value is calculated, they are not intended for mass standardised trading.

Furthermore the value is derived from a basket of independent assets that are not actually held or used as collateral in this calculation. In other words there are no assets that are held which underwrite the value of Totem Transactions. The value is primarily calculated for accounting purposes in the application itself.

Totem Transactions have been self-issued since April 2019 and although it has not been publicised, given the fact that a Crowdfunding will occur there may be an expectation of a claim on the MainNet for non-paying Contributors.

#### Test 7 Conclusion

Totem Transactions (ticker symbol: TOTEM) may be considered uncertificated securities under the Swiss Stock Exchange Act (SESTA). Self-issued uncertificated securities in Switzerland are esentially unregulated even if uncertificated securities qualify as securities within the meaning of the Financial Market Infrastructure Act (FIMIA). 

Totem Transactions (ticker symbol: TOTEM) are not intended to be traded on the Primary [Stock] Market and therefore do not need to be regulated in accordance with directives in Article 3 paragrah 3 [Stock Exchange Ordinance] of SESTA.

The issuance of Totem TRansactions are not analgous to Equity or Bonds and although FINMA has no direct responsibility in this area, the Token is not required to conform to the Swiss Code of Obligations and the Financial Services Act (FINSA).

---

### ANALYSIS 8. Other considerations under Swiss Regulations

#### Deposits

This issuance of Totem Transactions by the Live Accounting Association (Swiss Verein in formation) in exchange does not constitute a claim for the repayment of funds, and therefore the contributions are not considered deposits. There is no requirement under the Banking Act to require a licence. 

#### Collective Investment Schemes

The funds collected by the Live Accounting Association (Swiss Verein in formation) are not to be managed by third parties and are therefore is not a collective investment scheme.

# KYC and AML Regulations

In Switzerland in the case of utility tokens anti-money laundering regulation is not applicable as long as the main reason for issuing the tokens is to provide access rights to a non-financial application of blockchain technology (see Art. 2 para. 2 let. a no. 3 AMLO, FINMA Circ. 11/1 "Financial intermediation under AMLA" margin no. 13 et seq.).

However, in the interest of transparency the Live Accounting Association (Swiss Verein in formation) has taken a pragmatic approach in deciding to capture some personal information in order to satisfy requests from third-countries law enforcement agencies and to protect the Association, its directors and members. 

In the interests of securing the Personal Identification Information (PII) the following proceedure will be followed: 

* The application found at the domain https://totem.live will manage Contributors' registration.

* The data that is submitted by users will not be accessible by the application servers - with the exception of the pay-to cryptocurrency address that is sent to registrants.

* The details will be encrypted on the client side (i.e. on the registrant's own device) not by our Totem Accounting's servers.

* This encryption is in addition to the encryption that takes place via the secure websocket using the Secure Websocket Protocol `wss://` and a valid CA certificate. 

    * In this way only the Live Accounting Association (Swiss Verein in formation)'s public encryption key is used for encryption and therefore Totem Accounting or any thrid party cannot view or decrypt the registrant's personal data.

* This also means that registrant's PII can be transmitted securely over the Internet and stored securely on the Live Accounting database without needing to decrypt it.

* All intermediate servers including the Totem Accounting Messaging Server will not be able to decrypt the information as they will not have access to the decryption keys of the Live Accounting Association (Swiss Verein in formation). 

* All Personal Identification Informtion (PII) will be held in this encrypted format on the Live Accounting Association (Swiss Verein in formation) database. 

* The Live Accounting Association (Swiss Verein in formation) will not decrypt the data other than for verification purposes or following a formal written request made to the Live Accounting Association (Swiss Verein in formation) by law enforcement. 

    * As the Live Accounting Association (Swiss Verein in formation) is not obliged to go through this process all law enforcement requests will be made public by the Live Accounting Association (Swiss Verein in formation) stating which agency, when and for what reason they were requested.

    * The data provided in these requests will only ever be decrypted on a case-by-case basis and not decrypted _en masse_.

All Contributors who do not provide valid PII may be excluded from receiving TOTEM allocations on the Totem MainNet regardless of their contribution. Supplying false information is therefore at the risk of the registrant.

The personal Information that we request is as follows:

* Given name and Family name

* Residential Address including Postcode and Country
    * Residents of certain countries are excluded from participating in the Crowdfunding, but ***are not excluded from using the app***. See the [Crowdfunding Terms](Crowdfunding.md).

* Email address
    * Registrants' email addresses will be shared by the Live Accounting Association (Swiss Verein in formation) with the Totem Accounting and subscribed to the mailing list in order to facilitate announcements about the Crowdfunding progress and other news or relevant communications.