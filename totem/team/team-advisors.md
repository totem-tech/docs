# Totem’s Winning Team

## Advisors

### Luca Merolla
Managing Partner - Guru Capital SA.

### Ed Thomson
Former Web3 Development Grants Manager.

### Mike Tivey
Partner at EPGroup.

### Stephen Packer
CFO Equagroup.

