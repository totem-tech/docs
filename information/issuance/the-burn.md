# The Burn 

### The intention is that KAPEX holders will become the liquidity providers for the MainNet. 

_The intention is that the amount of TOTEM created on MainNet is dependent on the performance of the KAPEX Parachain Network and is of course subject to change without notice._

**In order to balance the network incentives about half of the supply created at MainNet will be allocated to KAPEX holders and the remainder will go to the Network Treasury as the liquidity provider of last resort.**

> **Quantity Minted By Burn + Early Adopter Migration Bonus** is the equivalent of 49% of total supply.
>
> **Network Treasury Allocation** is the equivalent of 51% of total supply.

## Quantity Minted By Burn Calculation

| Calculation                             |               |                |  |                        |
|-----------------------------------------|---------------|----------------|--|------------------------|
| The deterministic exchange rate for TOTEM | multiplied by | 0.02           | = | Discount Price           |
| Market Price of KAPEX                   | divided by    | Discount Price | = | Quantity Minted By Burn |


## Early Adopter Migration Bonus Calculation

This will be programmed into the genesis block.

| Calculation                             |               |                | |                         |
|-----------------------------------------|---------------|----------------|-|-------------------------|
| Totem User’s KAPEX balances             | multiplied by | 50 | = |         Early Adopter Migration Bonus           |


## The Network Treasury

This will initially be controlled by the Live Accounting Association (Swiss Verein in formation), and will later be community owned. 

**Note:** The Live Accounting Association (Swiss Verein in formation) will not migrate the balance of tokens from its holdings, but instead the Network Treasury will be allocated an amount equivalent to 51% of the coins to be minted at MainNet according to the price calculation above.

The Treasury is intended to be the liquidity provider of last resort in the initial startup phases of MainNet. The funds will be used to provide liquidity to users, in the exceptional circumstances where the holders of TOTEM refuse to provide liquidity to commercial users. It is intended as a safety mechanism to prevent users from being held to ransom.

Furthermore, in order to keep the supply safe the Treasury will receive 5% of transaction fees with the other 95% of fees going to validators on the MainNet.