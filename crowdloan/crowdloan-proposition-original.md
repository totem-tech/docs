

## Crowdloan Deal at-a-glance

> **There will be no vesting period following the Totem Crowdloan** - KAPEX coins will be available to transfer as soon as the balance transfer lock is disabled.
>
> Minimum Crowdloan Participation is **5 DOT.**


### KAPEX Coin Supply & Distribution

|                                                      | Supply % | KAPEX       |
|------------------------------------------------------|---------:|------------:|
| **Total KAPEX Supply**                               | **100%**     | **2,581,818** |
| KAPEX Released for Crowdloan & Pledge                    | 55%      | 1,420,000 |
| Development & Engineering Grants | 20%      | 516,364 |
| Airdrop (User Migration from Meccano Network*)                    | 3%       |  77,455  |
| Polkadot Community                         | 2%       |  51,636  |
| Current & Future Team                                | 20%      | 516,364 |

*_see terms of migration_

| The Pledge (See below)               |      |
|--------------------------------------|:------------|
| Min Pledge Participation             |  0 DOT |
| Max Pledge Participation             |  Up to 10% of Crowdloan Contribution |

### Crowdloan Referral Bonus

To encourage hitting the Soft and Target Caps all Crowdlenders can refer friends and receive a bonus based on their friend's contribution

|                                       | Referee Bonus | Referer Bonus
|--------------------------------------|------------:|-----------------:|
| Referral paid in KAPEX based on referee contribution          |  5%         | 5% |

_Users who have been referred, must make a contribuition to the Crowdloan, to activate the referral bonus for themselves and the person that referred them. Referral only extends directly to the referred party and is measured by the referred party’s contribution. In other words the bonus does not extend or accumulate to the initial referer when the referee is themselves a referer._


### Soft Cap & Success Bonus

**If the Soft Cap is reached all users will automatically get a bonus.**

|                                      | DOT Qty     | KAPEX Allocation |
|--------------------------------------|------------:|-----------------:|
| Soft Cap for the Crowdloan           |  1,000,000  |  100,000         |
| Soft Cap for The Pledge              |  100,000    |  10,000          |
| Contributor Bonus<br />[Crowdloan Soft Cap Reached] |  10%           |  11,000          |

### Target Cap & Success Bonus

**If the Target Cap is reached all users will automatically get a bonus.** *

*_This bonus replaces the Soft Cap Bonus (or else you can think of it as a further 10% bonus on top of the Soft Cap Bonus)._

It is intended that if the Target Cap is reached, no more Crowdloan funds will be accepted, however participants will still be able to pledge funds up to the maximum permitted amount.

|                                      | DOT Qty     | KAPEX Allocation |
|--------------------------------------|------------:|-----------------:|
| Target Cap for the Crowdloan                | 10,000,000 |  1,000,000       |
| Target Cap for The Pledge              | 1,000,000 |  100,000         |
| Contributor Bonus [Crowdloan Target Cap Reached]  | 20%            |  220,000         |


> **Disclaimer: These pages do not constitute investment advice. You should do your own research, and should not participate in this process if you do not understand or feel uncomfortable about committing to the requirements of this program. We offer no guarantees of return of investment [ROI] as DOT prices may vary along with the price of KAPEX. However all DOT committed to the Crowdloan are fully refunded after either the lease period ends or the Crowdloan period whichever is the longer.**

### Payouts

**There will be two phases to the payouts:**

#### Phase I.a Initial Payout

This initial distribution will be based upon the total contributions in the Crowdloan as logged by the Polkadot Relaychain Crowdloan module.

This will be carried out after the Kapex chain wins a parachain slot and becomes live. Transactions will be executed to distribute the funds directly to contributor's wallets.

Crowdloan referral bonuses will not be paid at this stage, because the next phases are required to calculate the full bonus payout to contributors even if they were referred.

For existing qualifying users of the Totem Meccano Network, they will receive their migration allocation in Phase II based on their balance in Meccano and their referral bonuses already received. The conversion ratio will be announced once the number of accounts and amounts to migrate are known. There will also be a cut-off date for awards that can be earned in Meccano.

#### Phase I.b Pledge Contribution

The Pledge contributions will then be collected from all participants. We will announce the mechnaism for this step shortly.

#### Phase II Distribution of Bonuses

This is the payout stage for both the Pledge contributions and the final total bonuses earned by contributors.

At this point all the necessary information is available to calculate and assess the Cap achievements, contributor bonuses and referral rewards. 

**_In the following pages we outline how you can participate in the Crowdloan_**