# Calculations for Totem KAPEX Allocations

**NOTE This page has been updated. You can see the original version [here](/crowdloan/crowdloan-details-original.md).**

In this section we outline the details of the allocation mechanisms for Totem’s KAPEX Parachain Crowdloan and Pledge.

### Base Calculation

> Minimum Crowdloan Participation :  **5 DOT** <br />
> For every **1 DOT contributed in the Crowdloan** you will receive a minimum of **0.1 KAPEX** plus <br /> a share of the [Hard Cap Allocation](/crowdloan/crowdloan-proposition?id=contribution-guarantee) of 568,000 KAPEX.<br />
> _This does not include bonuses (see below)._

**The total quantity of KAPEX minted is 2,581,818 KAPEX coins.** 

* **55% of all coins - 1,420,000 KAPEX** are available to supporters of the Crowdloan and Pledge.

* If we reach our **Soft Cap Crowdloan contribution of 568,000 DOT** we will award all participants a **10%** increase in awards for all funds contributed to both the Crowdloan and the Pledge.

* If we reach our **Target Cap Crowdloan contribution of 1,775,000 DOT** we will award all participants a further **10%** increase in awards, making a **total 20% bonus** for all funds contributed to both the Crowdloan and the Pledge.

### The Referral Bonus 

Each participant in the Crowdloan can also benefit from referring a friend, and when that friend contributes in the Crowdloan, you will both receive a split bonus of 5% of the contributed funds in the crowdloan. 

* this only applies to the amounts committed into the Crowdloan - you will not receive a bonus on any amounts pledged by your friend.

### The Pledge

**Reminder: The Pledge is a non-binding signal of intent to help fund the development team, actionable only if a parachain slot is won. DO NOT SEND FUNDS when you Pledge.**

Totem is a small highly motivated development team. We have been founder funded since we began building our network in 2018, and now we are ready to grow our team, and accelerate the development to market.

You can chose to **pledge up to a maximum 100% of the quantity of DOT committed in the Crowdloan** provided that the Target Cap is not surpassed.

> Bonus Bonus: The Pledge amount is **also included in the calculation for Soft and Target Cap bonuses!**

#### Pledge Award Calculation

By signalling an amount of DOT in the Pledge, you will be able to claim a multiplier of Totem KAPEX calculated as follows:

> Minimum Crowdloan Participation :  **0 DOT** <br />
> For every **1 DOT contributed in the Pledge** you will receive a minimum of **0.32 KAPEX**.<br />
> _This does not include bonuses, which are added on top dependent on reaching the Soft or Target Caps (see above)_.

### Soft and Target Cap Success Bonus

Regardless of whether you participated in the pledge or not, the total DOT contributed is the basis for the *Cap Success Bonus calculation.

### Airdrop

Totem’s Meccano userbase will have their balances migrated to the KAPEX network allocating the 77,455 KAPEX prorata basis. 

## Other information

**Commitment Limits**
* For the Crowdloan and Pledge there are two upper commitment limits (Hard Caps): 5,680,000 DOT for the Crowdloan and 1,775,000 DOT for the Pledge. 
* The Pledge has no lower limit of contribution as it is voluntary.
* The Ratio of commitment is capped at 100% of the specific Crowdloan commitment per address. 
  * The [Crowdloan Dapp](https://totem.live/crowdloan/) enforces these limits.

_There are implied hard limits to participation proportional to the Crowdloan limits and the Pledge limits set out above._

**Lock Periods**
* The DOT contributed towards the Crowdloan are locked for the period of the parachain lease and can be reclaimed thereafter (96 weeks).
* KAPEX is awarded if the parachain Slot auction is won, and will be carried out on chain.
* A balance transfer lock will apply to all KAPEX awards for a period of time following a parachain win. This will give the team time to calculate and distribute the funds and apply it to the participating Polkadot addresses.
* Once the allocations of KAPEX have been completed, transfers will be enabled. 

## Overall Supply Allocations

In the following tables we set out how we have configured the allocations for the Crowdloan and Pledge.

| **Distribution**                                       | **KAPEX**        | **%age** |
|--------------------------------------------------------|-----------------:|---------:|
| Max Supply KAPEX                                       | 2581818  KAPEX   | 100%     |
| KAPEX Released Crowdloan & Pledge Maximum Distribution | 1420000  KAPEX   | 55%      |
| ...of which Minimum Guaranteed Distribution            | 568000   KAPEX   | 22%      |


### Soft, Target and Hard Cap Allocations and Referral Bonus Allocations

|                              | **DOT**      | **Allocation of KAPEX**       | **%age Bonus** | **Bonus Amount** |
|------------------------------|-------------:|-------------------------------:|----:|-------------:|
| Soft Cap In Crowdloan        | 568000  DOTS | Pro-rata share of 568000 KAPEX | 10% | 5680 KAPEX   |
| Target Cap In Crowdloan      | 1775000 DOTS | Pro-rata share of 568000 KAPEX | 20% | 35500 KAPEX  |
| Hard Cap In Crowdloan        | 5680000 DOTS | Pro-rata share of 568000 KAPEX | 20% | 113600 KAPEX |
|                              |              |                                |     |              |
| Soft Cap Pledge              | 177500  DOTS | 56800 KAPEX                    | 10% | 5680 KAPEX   |
| Hard Cap Pledge              | 1775000 DOTS | 568000 KAPEX                   | 20% | 113600 KAPEX |
|                              |              |                                |     |              |
| Referer Bonus 10% of HardCap |              | 56800 KAPEX                    |     |              |

*_There are no limits to the amounts you can commit, and the values are calculated at the time of writing. They calculations may be subject to change up to 48 hours before the KAPEX Crowdloan starts._

### Pledge Use of Funds

Once the pledges are collected the Live Accounbting Association will distribute the funds in the following way:

|                                           | Distribution % |
|-------------------------------------------|----------------|
| Development & Engineering Grant Program   | 39%            |
| Staff Network & Operations                | 35%            |
| Marketing                                 | 15%            |
| Contingency                               | 10%            |
| Travel                                    | 1%             |