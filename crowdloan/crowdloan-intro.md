> **If you have not seen these pages before, feel free to browse the [Totem Live Accounting](/) documentation links in the sidebar and visit our [Website](https://totemaccounting.com) and [Meccano Test Network App](https://totem.live).**

# Announcing the Totem Parachain & Crowdloan.

<img src="/_media/totem-polkadot.png">

### Totem Live Accounting Team is excited to announce that after more than two and a half year’s development in the Polkadot Ecosystem, we are ready for our Crowdloan offering for the Polkadot Parachain Slot Auctions.


## What is a Parachain?

**A parachain is not a smart contract. It is a seperate blockchain network, that will connect to the Polkadot blockchain network.**

Totem KAPEX Parachain is such a blockchain. In order to connect KAPEX to Polkadot funds will need to be raised to pay for a lease - or a slot - on the Polkadot Network for a fixed period of time. 

## Ok then, what is Polkadot?

**Polkadot is also a blockchain. However because of its relationship to the other blockchains connected to it, Polkadot is often referred to as a Relay Chain, or Layer 0 chain.**

It provides a number of important functions for the chains that are connected to it:

* Security and immutability for the (para)chains that are connected to it. This is the main reason for projects to want their chains to be connected to Polkadot. 

* A way for all the chains to relay messages between each other. A good example would be in transferring coins between chains without requiring an exchange.

For more detailed information head over to the [Polkadot Wiki](https://wiki.polkadot.network/).

> Polkadot is really important in the blockchain space because it solves the scalling problems that have beset smart contract blockchains like Ethereum. 

In Ethereum and other smart contract platforms the business logic in smart contracts are all competing for limited resources - the limit of the amount of execution time to be included in a block. Not only is this a problem in terms of scaling - it is also a massive problem in terms of network fees and transaction costs.

However in the Polkadot network all of the business logic exists outside of Polkadot. In fact it is designed so that each blockchain should have its own specific business logic perhaps not even related to other chains. 

This means that the execution of the business logic does not happen on Polkadot, and therefore it does not compete for resources thereby solving both the scaling problem and the trabsaction fee problem all at once. 

## How is this related to Totem?

**Totem has been built from the outset with the intention of connecting to Polkadot to receive the benefits outlined above.**

<!-- Although our App is running independently on our [Totem Meccano Canary Network (Testnet)](information/the-networks), we have already prepared our App to be able to connect directly to the [Totem KAPEX Parachain Network](information/the-networks) running on Polkadot. -->

<!-- The following slide from our presentation at the very first [Polkadot Sub0 developer conference in April 2019](https://youtu.be/kXYhW7zkAMY), is still how we see this playing out. All of the various Totem Instances being able to communicate with each other via [Polkadot Cross-Chain Messaging](https://wiki.polkadot.network/docs/learn-crosschain).   -->

<!-- <img src="/_media/sub-0-polkadot-network.png"> -->

In order to join the Polkadot Network the Totem team has had to create a parachain (which we call KAPEX) and will need to secure a Parachain Slot Lease. The cost of a lease can be in the region on **$50M-$200M** and no team is expected to have that spare amount of money available to secure a slot. 

> The design of Polkadot is that parachain slots are funded by the community via **_Crowdloans_**. 

**Simply put, Totem will need a Crowdloan in order to connect the Totem KAPEX Network to Polkadot as a Parachain.**

## What is a Crowdloan?

The reason why it is a _loan_ is because the _crowd_ of people that fund the slot on behalf of the project team **never lose their money - they get it back at the end of the lease period.**

In exchange for this support, the project team typically grants native coins from their network to the contributors in the crowdloan.

> It works roughly like this: for every `$` committed to the crowdloan, they will receive back more-or-less the equivalent `$` in project network currency. **The great thing is that they will always get their original DOT contributions back after the lease period is over or if the team fail to win a slot.**

Each project team has its own Crowdloan proposition and Totem is no different. 

* These pages explain Totem’s KAPEX Parachain Crowdloan proposition.

Each project team must take part in an Auction process to win a slot connecting to Polkadot. 

* In order to bid on an auction, the mechanism uses the funds raised in the Crowdloan to make the bids - but you can be sure the team itself **_cannot touch your money_**. Polkadot does not allow this to happen.

**_In the next sections Totem sets out its proposition for the community_**
