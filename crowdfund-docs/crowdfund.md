# Welcome to Totem’s Crowdfunding

### Over the past two years, the development of the Totem Live Accounting App has been completely founder-funded. 

> Today, the project has reached a critical growth stage in our Roadmap towards Totem Live Accounting release on MainNet.

**We’re announcing a Crowdfunding seed round to fund expansion of the current team and to accelerate development of the Live Accounting App.The community will have an opportunity to become stakeholders in the Totem Live Network.** 

Community members can participate in the funding round by buying batches of the network currency _**Totem Transactions (ticker symbol: TOTEM)**_ on the current Testnet.  

---

***All persons intending to participate in the Crowdfunding must read, understand and agree to the*** 
[***Contribution Terms***](/Crowdfunding-docs/contribution-terms#contribution-terms)

---

<!-- You can register [here](https://totem.live) -->

#### Note: 

_Totem Transactions_ which are distributed during the Crowdfunding, are unlike any other cryptocurrency because they have a number of emergent properties that cannot be reproduced on other blockchain networks. These properties appear because the currency is coupled with accounting software.

**Read an Overview of the offering in the next section.**