

# Free allocations of TOTEM network tokens:

### All available coins not already allocated to _users_ in the Meccano Testnet and not accidentally burnt in the development process will be re-assigned to the Network Treasury address. This includes all founder allocations that were made at the genesis block. 

> In other words the Founders will give up their allocations on the Testnet in order not to skew allocations on the KAPEX Parachain.

* The Project Team will make adjustments to existing users’ balances in the Testnet which will be carried over to KAPEX Parachain:

    * Current users of Totem are already in posession of tokens from the faucet (circa $0.17) will receive a bonus of the equivalent of $25. [Done!]

* The Network Treasury address will provide funds to the faucet.

    * New users will receive a faucet allocation of the equivalent of $5 [Done!]

    * Users can refer friends and will receive a bonus of  the equivalent of $5 per referred friend from the faucet. [Done!]

        * The referee will aslo receive a bonus of $5 from the faucet. [Done!]

        * A referee can only make a maximum of 15 referrals per week. [Done!]

        * User can earn significantly more if they use the Twitter reward mechanism. [Done!]

> **All users signed up on the KAPEX Parachain and who have actually used Totem Live Accounting on KAPEX will receive a 50x multiplyer when their balance is migrated to MainNet.**

**In the next section we provide information about paid allocations of TOTEM.**