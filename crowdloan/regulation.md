# Regulation in relation to Accounting Software.

> The Totem Team are building accounting software. 

As such "accounting software" does not require compliance with any known regulatory directive anywhere in the world and therefore the software is free from regulatory oversight.

In addition, the software employes a global Unit of Account as the underlying "functional currency" in the sense of [FASB 52](https://www.fasb.org/page/PageContent?pageId=/reference-library/superseded-standards/summary-of-statement-no-52.html&bcpath=tff), [IAS 21](https://www.iasplus.com/en/standards/ias/ias21). 

It is a fact that a global accounting software application cannot operate in any meaningful mathematically correct sense without a functional currency mechanism - it is an intrinsic mechanism of the software itself.

> The Totem Live Accounting Software and Protocols in its fundamental operation as an accounting software system is not subject to any known regulatory scrutiny in any jurisdiction at the time of writing*.

_*updated December 12 2022._

---

## Regulation, The Howey Test and Swiss FINMA

#### Whilst our work is primarily the construction of software we are aware that Financial Regulators tend to scrutinise blockchain projects to determine if a token mechanic is a security or not. In this section we provide an analysis of the project from this perspective to address any concerns that partners and contributors may have.

We explore the so-called **"Howey Test"** and **Know Your Customer (KYC) and Anti-Money Laundering (AML) Compliance** particularly in relation to the Crowdfunding/Pledge and Parachain Slot Auction as well as the historical context of use in the Test Network and as a functional currency for accounting purposes. 

In addition as the Live Accounting Association (Swiss Verein in formation), based in Switzerland is administering the Crowdfunding/Pledge and Parachain Slot Auction we examine the requirement to follow the Swiss Financial Market's Authority Guidance as outlined below.

---

**_Do not register or contribute to the Crowdfunding/Pledge and Parachain Slot Auction if you do not understand anything that is documented here or in the other Crowdfunding/Pledge and Parachain Slot Auction pages._**

_The opinions stated below are not legally binding and may be subject to change without notice in the event that regulatory conditions change._

---

## The Howey Test 

The Howey Test which came out of the 1946 Securities and Exchange Commission Court Case  [Howey vs SEC](https://en.wikipedia.org/wiki/SEC_v._W._J._Howey_Co.) has become widely accepted as  being applicable to all cryptocurrency fundraising efforts since 2017. 

The purpose of the Howey Test is to determine if the Crowdloan and Pledge constitutes an "investment contract".

The Howey Test is set out in four parts. All four parts need to be passed in order for the distribution of Tokens to be considered an "investment contract" and therefore as a security. If one or more parts fail, then the Token _may not_ be considered a security in most jurisdictions.

If the Token is deemed to be a security (i.e. it passes all four tests), then the sale must register with the Securities and Exchange Commission in the USA, regardless of the country in which the Token sale occurs.

> It should be noted that whilst many projects have "tokens" issued by smart contracts, this is not the case for any of the Totem Live Accounting Networks because they each have their own blockchains with native "coins" used to pay transaction fees. Furthermore, the "coin" and the extra-regulatory "functional currency" of the accounting system, are in fact one-and-the-same Unit of Account.

The four parts of the Howey Test are broadly:

* Is there an investment of money?
* Is there a so-called "Common Enterprise"?
* Is there an expectation of profits or returns on investment?
* Do returns rely solely on the efforts of others?

In the following sections we detail how these tests apply to this Crowdloan and Pledge. Please read this very carefully, and if you have any questions you may reach out to us on any channel for clarification.

#### Glossary

* The **Totem Live Accounting Meccano Canary Test Network ("the Testnet")** is a public blockchain network that has been live since February 14th 2019, and has been the backbone of all the development work that has taken place before and after it was launched.

* The **Totem Kapex Parachain Network ("Kapex")** was connected to the Polkadot Network on 29th April 2022, but did not function until a decentralised auction had been won on the Polkadot Network. 

    * The Kapex project is a public blockchain, and due to it's network relationship with Polkadot it is referred to as a "Parachain".

* A pseudo-funding round took place in order to win the auction on Polkadot. It was named the **"Crowdloan & Pledge"**.

    * The purpose of the Crowdloan was to allow the holders of the Polkadot "DOT" cryptocurrency to voluntarily put up a fully-refundable "lease guarantee" on behalf of the Totem Team in order to win an auction and secure a slot for the Kapex Network on Polkadot. 

    * The process of fundraising in a Crowdloan is "arms-length" and cannot be controlled by the Totem Team directly. They neither have access to the funds that are provided by the lease guarantors who take part in the Polkadot Crowdloan nor are they able prevent a return of funds to those guarantors once the lease expires.

    * The Pledge did allow for contributors to the Crowdloan an additional ability to offer non-returnable DOT funds to support the team.

* In both cases the native coin of the parachain was given free-of-charge to participants who made contributions to the Crowdloan, whether or not they decided to take part in the Pledge.

* The **Totem Mainnet ("Mainnet")** has yet to launch.

#### Prior Distribution 

* All users who have registered on the [Totem Live App](https://totem.live) ("the Dapp") connect directly with the Testnet and have been allocated cryptocurrency (or "coins") free-of-charge using the automated Testnet "Faucet" since April 2019.

* The Testnet does not depend on other blockchains such as Ethereum or Bitcoin - it is a completely independent "layer 1" blockchain network. 

* The Testnet coins are consumed in order to execute actions on the Testnet. For example to create Activities, Timekeeping and Tasks and to administer them requires you to spend the Testnet currency to process these requests on the blockchain.

* Users of the Testnet since launch have also been able to request, with certain limits, further allocations of coins from the Testnet Faucet without charge. 

    * This request is carried out from within the application itself. 
    * These additional allocations have been fixed at the same level as the initial allocation and have been distributed free-of-charge.

* The allocations made to users will be conditionally carried over to the Totem Kapex Parachain Network as a reward for early adopters at an exchange rate to be decided. 
    
    * Users will be required to claim their share of the Kapex Network coins allocated for this purpose as they will not be paid directly.

* The network currency used on the Testnet is a cryptocurrency that has never been intended to have a value determined by after-market exchanges. 

    * We are not aware of any exchanges that have listed this network coin nor has the Totem Team made any application to have this coin listed on third-party exchanges.

        * It should be noted that as the Totem networks are permissionless blockchain networks, no party can prevent after-market exchanges from listing any of the coins associated with the networks described here. 

* It is intended that Testnet Coins that are migrated to the Kapex network will also have the ability to be migrated to the Mainnet in a future iteration of the project. 

* No guarantees have been given regarding the launch of Mainnet. 

    * The intention however is that Kapex coins do not automatically migrate, but must be burnt (a term meaning "destroyed" or otherwise become permanently unspendable) in order for the owners to receive an equivalent quantity of new coins on the Mainnet.

        * This is not a two-way process - once the coins have been migrated to the MainNet, they cannot be returned to the Kapex Network.

* The Testnet currency has been designed from the outset to explore and research a value calculated deterministically by an automated algorithm as set out in our 2016 Paper and more recently updated. The algorithm determines the virtual value of the Testnet currency is based on information available to the runtime of the test network.

    * The concept provides that the virtual value of the coin cannot be manipulated by external parties and allows accounting entries to be made with a "Unit of Account of deterministic value" rather than a currency of floating value. 
    
    * This primarily enables accounting operations to be recorded with the Unit of Account and not with a coin at market price which would have undesirable volatile characteristics.

    * This Unit of Account value determination applies to the Testnet, and will also apply to the Mainnet. 

    * The coins on the Kapex network will be subject to market pricing.
        
        * This implies that the Kapex coin itself will not necessarily be a useful Unit of Account as it will be subject to market volatility. 

* The quantity of the the Testnet currency allocated by the Testnet Faucet is in accordance with the valuation algorithm described in [the documentation](https://docs.totemaccounting.com/#/information/overview-token-3?id=steps-to-calculate-totem-exchange-rate). 

* At the time of the Testnet start, for accounting purposes as a Unit of Account (i.e. not a valuation) quantity of 16181 coins was apprimately equivalent to 0.18 USD, and was used this way in order to determine what the accounting entry amount should be stated in the network currency not the fiat currency. 

    * This amount of coins allocated was thought to be sufficient for micro-business to conduct accounting operations and business activities in the Testnet Dapp essentially for free for a meaningful period of time without requiring an investment of fiat or cryptocurrency funds.

* In the case of a request to replenish the Testnet currency, the Testnet Faucet allocated the funds unconditionally and free-of-charge until it was deprecated on 29th April 2022.

#### Administration of Distribution of Coins following the Crowdloan & Pledge.

The Crowdloan and Pledge is administered by the Live Accounting Association (Swiss Verein in formation). The basis of the allocation of Kapex coins has been set out in the Crowdloan details elsewhere in these documents. 

As the contributors are always in control of their funds, the Kapex coins that are allocated in proportion to the contribution of DOT are essentially given away free-of-charge.

* The contribution of DOT in the Crowdloan or Pledge does not constitute or confer membership of the Association. 

* The Live Accounting Association (Swiss Verein in formation) will set aside money received as a donation to enable the purpose of the Association, but will otherwise rely on the Kapex currency for funding that purpose.

> The purpose of the Live Accounting Association (Swiss Verein in formation) is to provide funding for the development of the Live Accounting Networks. Any developer - not just the Founders of Totem Accounting - can apply for grants from the Association for work that carries out the purpose of the Association.

* Founders and team will receive an allocation of Kapex from the Live Accounting Treasury.

* In future all balances will be transferred to Totem Live Accounting MainNet at a minimum of 1:1 ratio of allocation the Kapex currency.

### ANALYSIS 1: Is there an investment of money?

The first test is unlikely to be satified as the Testnet cryptocurrency was distributed on an already live network via an already live application, free-of-charge and for a significant period of time prior the Crowdloan and Pledge. These coins are also conditionally claimable on the Kapex network free-of-charge.

The Kapex coins distributed to the Polkadot Crowdloan participants are given on a free-of-charge basis, as the contributors get to keep both the DOT contributed and Kapex that was allocated unconditionally. 

The DOT provided in the Pledge are considered donations to the Live Accounting Association, and not a purchase of Kapex coins. The Pledge was only available to participants of the Crowdloan as a means of augmenting the number of Kapex that was allocated to them and not open to private investors to purchase the coins.

#### Howey Test 1 Conclusion 

No investment of money was ever required to obtain both the Testnet and Kapex coins. Therefore Kapex does not qualify as a Security. Consideration must still be given to the other tests.

---

### ANALYSIS 2: Is there a so-called "Common Enterprise"?

As the Testnet has already been developed and is already live, there is no requirement for the Contributors to the Crowdloan and Pledge to be part of a common enterprise in order for the project to be developed, launched or to succeed.

#### Howey Test 2 Conclusion

The second test fails to establish a common enterprise, as Contributors are participating after the application has been developed and available to use.

---

### ANALYSIS 3. Is there an expectation of profits or returns on investment?

> Since Howey Test #1 established that there is no investmeent of money this point is irrelevant.

There are three categories of Testnet currency owners:

* Early users / owners who were allocated the Testnet currency from the Testnet Faucet and can claim Kapex currency on the Kapex network.

* Crowdloan and Pledge Contributors who are to be allocated Kapex currency as a function of their contribution amounts.

* Owners who are both of the above

For users allocated Testnet currency from the Testnet Faucet alone there is no expectation of profit. No investment was ever collected and no publicity has ever been made to this effect at any time whatsoever.

The Kapex currency that is allocated by the Crowdloan and Pledge process is allocated on a free-of-charge basis.

#### Howey Test 3 Conclusion 

In the absence of an investment of money, there can be no expectation of profits on the investment. The Totem Team has never promoted the idea that a profit could be made from contributing to the Crowdloan and Pledge.

Therefore there is a failure to establish such an expectation of profit for holders of the coins.

---

### ANALYSIS 4. Do returns rely solely on the efforts of others?

In traditional so-called Initial Coin Offerings (ICO) often the project is just an idea. At the point of the ICO the smart contract has not been developed yet, and may fail if the development team is not able to achieve their goals. In these cases it is clear that investments in ICOs would require others to do work in order to realise a profit or return.

The Totem Accounting development team has been entirely Founder-funded until this point and the project began some time in 2016.

> The Totem Accounting development team have developed the blockchain network and the core functionality of the application long before requesting support from the community. 

Potential contributors were in a vastly different position with Totem Live Accounting Application as they were able to make informed judgements about the team and its abilities to deliver and build their product based on what they have achieved and delivered before contributors could participate.

**_The contributions they made were aimed at securing a Polkadot Slot lease and not to "develop an idea"._** In essence Totem Live Accounting was already a long way down the road to becoming a full product.

#### Test 4 Conclusion 

The Founding Team has already completed the work to enable the network. It was no longer dependent on their efforts to function or operate. However, it is dependent on their vision and credibility to accelerate the goal towards widespread adoption.

The network is already functional, the code is open source, and the documentation about the project aims are is in the public domain. 

These facts alone mean that the Howey Test is not satified on this point and that the enterprise did not rely on the efforts of others. 

---

## Howey Test Conclusions

Long before the Crowdloan and Pledge, the Testnet and Dapp became public and operational with the core functionality having already been built, launched and available to the public.

Furthermore the Testnet currency has been issued and distributed for a significant period of time prior to the Crowdloan & Pledge free-of-charge. 

In a limited capacity this free delivery will continue after the Crowdloan and Pledge completes using so-called "airdrops" for marketing purposes and community engagement.

The users of the Testnet (and eventually the Mainnet) are not required to carry out work in order to give value to the coin as the value is and will be determined by an independent algorithm rather than a third-party market price. The Kapex network will eventually have a value determined by the market.

The software application has almost entirely been completed by the developers prior to the Crowdloan and Pledge and can be used today although there are still many elements to be completed before a full beta product launch.

This analysis is subject to alternative interpretation however for the reasons set out above **_all four tests fail_**. On this basis the three network currencies **_are unlikely to considered a security in the United States of America_** or elsewhere.

**This opinion may change in future or should the legislation change.**

---

# Swiss FINMA Regulations

Under the Swiss Markets authority Guidance (FINMA Guidance 04/2017), the Totem network currencies may fall under one or several categories of Token:

* A Payment Token
* A Registered or Unregistered Security Token
* An Asset Token

The categorisation is not mutually exclusive and can be cumulative.

A further categorization is possible, the so-called Pre-Sale or Pre-Financing of a project. In the case of The Testnet currency which is already in circulation this categorisation is not applicable.

### ANALYSIS 5. Are Totem Network Coins Payment Tokens?

Totem network currencies are in the classic sense cryptocurrencies. They are intended to be consummed when transactions are executed on a blockchain network, and can be used for payments. The quantity of the network currencies that is consumed by a transaction depends on the byte size of the transaction and other technical factors including the type of accounting record to be stored on the blockchain.

However, unlike traditional cryptocurrencies the value of the Testnet currency and the future Mainnet currency is deterministic, as it is designed to be used as a Unit of Account within the accounting engine. In this case the value that is recorded may not have any bearing on a specific blockchain transaction, and may not even be related to an asset that is held.

The Kapex network currency is intended to have a free-floating market value, and can be used for settlement.

#### Test 5 Conclusion

Totem network currencies are defined as a cryptocurrency and given the nature of the value calculation for the Testnet and Mainnet currencies they will inevitably be used as a settlement currency although this mechanism was primarily designed as a Unit of Account for valuation and accounting purposes. 

The Testnet currency and Mainnet currency and by implication Kapex network currency can be considered a Payment Token and is therefore not treated as a security in this context.

---

### ANALYSIS 6. Are Totem Network Currencies Utility Tokens?

By definition all three of the Totem network currencies are Utility Tokens whose purpose is to confer digital access rights to the blockchain network amonst other features.

#### Test 6 Conclusion

In this case the Totem network currencies can be considered utility coins but crucially exhibit missing properties that would otherwise class it as a security; for example to provide access to capital markets.

---

### ANALYSIS 7. Are Totem Network Currencies Asset Tokens?

Totem Testnet currency is distributed on an existing so-called Test Network environment. The coins are intended to be migrated to the Totem Kapex Parachain Network when it goes live. 

The allocation is conditional upon the contributor to comply with the terms of migration, but in general balances on accounts will be migrated plus any additional bonuses earned in the interim.

The Testnet coins are standardised and they are not intended for mass standardised trading.

Totem Testnet coins have been self-issued since 2019 and there is an expectation of claims on the KAPEX Parachain Network for free-of-charge testnet coins migrated from the Testnet to the Kapex Parachain.

Additionally the Kapex coins are also sef-issued since the network started producing blocks (i.e. went live) in August 2022. These coins will also be distributed free-of-charge according to the amount of DOT contributed towards securing the Polkadot Slot Lease.

#### Test 7 Conclusion

Totem Network Coins may be considered uncertificated securities under the Swiss Stock Exchange Act (SESTA). Self-issued uncertificated securities in Switzerland are esentially unregulated even if uncertificated securities qualify as securities within the meaning of the Financial Market Infrastructure Act (FIMIA).

Totem Network Coins are not intended to be traded on the Primary [Stock] Market and therefore do not need to be regulated in accordance with directives in Article 3 paragrah 3 [Stock Exchange Ordinance] of SESTA.

The issuance of Totem network currencies are not analgous to Equity or Bonds and although FINMA has no direct responsibility in this area, the network currencies are not required to conform to the Swiss Code of Obligations and the Financial Services Act (FINSA).

---

### ANALYSIS 8. Other considerations under Swiss Regulations

#### Deposits

This issuance of Totem network coins in exchange for contributions to the Crowdloan does constitute a claim for the repayment of funds however the Totem team are not the custodians of the funds, the Polkadot network is the custodian and the funds are automatically returned to the contributors own wallet addresses without any actions required from the team. Indeed the team are unable to touch these funds, redirect them or otherwise control them in any way. The claim, if any exists, is between the original holder of the funds and the Polkadot network. The contributions are therefore not considered deposits held on trust by the team. There is no requirement under the Banking Act to require a licence.

#### Collective Investment Schemes

The funds collected by the Live Accounting Association (Swiss Verein in formation) with regards to the Pledge are not to be managed by third parties and are therefore is not a collective investment scheme.

# KYC and AML Regulations

In Switzerland in the case of utility tokens anti-money laundering regulation is not applicable as long as the main reason for issuing the tokens is to provide access rights to a non-financial application of blockchain technology (see Art. 2 para. 2 let. a no. 3 AMLO, FINMA Circ. 11/1 "Financial intermediation under AMLA" margin no. 13 et seq.).

Since the project is building accounting software as such it is fully compliant with this regulation.