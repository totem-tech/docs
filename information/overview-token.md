

# Token Economics

<!-- ## Emergent Properties of TOTEM (Totem Transactions). -->

### The following pages explain in detail the emergent properties of the internal _Functional Currency_ of the Totem Live Accounting Blockchain Network and the economic model.

### **tl;dr**

_**Totem Transactions** - the native cryptocurrency of the app (ticker symbol: TOTEM) is a true Unit of Account because it is both a stable Functional Currency and it is **intrinsic to the Accounting Engine**. This may be the first time this has ever been conceived or executed - even for traditional accounting software._