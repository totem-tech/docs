### Main Crowdfunding Funding Caps

| Category                                                    | Lock Period | Allocation Value  | Amount in TOTEM         | Amount in bTOTEM |
|-------------------------------------------------------------|------------:|------------------:|----------------------:|---------------:|
| Pre-Crowdfunding Airdrop existing Users incl. Referral Scheme  | 0 months    | $ 10,000,000      | 214,983,417,839       | 2,150          |
| Crowdfunding                       | n/a         | $ 465,152,155     | 4,837,126,901,366     | 48,371         |
| Treasury                        | n/a         | $ 155,152,155     | 3,335,514,047,006     | 33,355         |

_We are using the Batch notation for convenience (1bTOTEM = 100,000,000 TOTEM)._


* The Pre-Crowdfunding Airdrop will be locked until the Crowdfunding is completed, after which time all funds in this category will be released to holders.

* The Project Team will control the following accounts:

    * Crowdfunding Allocations Account
        * This is the account that will administer the allocations in the Crowdfunding (see table below).
    * Treasury
        * This is the account that will administer any over-subscription in the Crowdfunding.

**[Go Back](Crowdfunding-docs/Crowdfunding-details.md)**