

# KAPEX Token

### Because we know that users like to own free market tokens we are issuing a special token on the KAPEX Parachain Network (ticker symbol: KAPEX) with a **_very_** limited supply.

> **The token supply will be limited to only  2,581,818 KAPEX coins.**

These tokens will be primarily issued in exchange for DOT contribution in the Totem KAPEX Parachain [Crowdloan](#). **Whatever you contribute to the Crowdloan is not taken by the Totem Team - you have full control of the DOT you contribute although it is locked for 96 weeks, and returned to you afterwards.**

For every DOT that is contributed, an allocation of KAPEX coins will be made to contributors. However the overall allocation of KAPEX coins from the total supply follows this schedule:

### 80% to Community of which:

* **55% for sale in the Crowdloan.**

    * Crowdloan Minimum Contribution **5 DOT**

    * Price per 1 DOT contributed: **0.1 KAPEX**.

### 20% to Current and future team

The purpose of the fundraising is to grow the team and accelerate development towards the MainNet launch. We will need to allocate some of these funds to new team members and incentive programs such as bounties and bug bounties.

### Remaining 25% distributed as follows:

* 20% Development and Engineering Grants Program Reserve.

* 3% UI Referrals Promotions and Airdrops.

* 2% Polkadot Community Teams.

## How do KAPEX Parachain Tokens relate to MainNet TOTEM Tokens?

On MainNet launch, KAPEX holders will burn their tokens to acquire MainNet currency (symbol: TOTEM).

* The market price of KAPEX coins at the time of the Burn will determine the quantity of TOTEM tokens minted - and this in turn will determine the initial supply on MainNet.

* As TOTEM has a stable value the mechanism of burning will ensure that the owner receives a 98% discount of the market price at burn time. This will also increase the supply on MainNet, whilst at the same time retaining the coin quantity/value for the KAPEX holder. (See the next secion for more details about **_The Burn_**)

    * The burn period will be time limited. 
    
    * After burn deadline, KAPEX coins can only be burned at the last market price on the deadline.

In the next secion we discuss the details about **_The Burn_**.