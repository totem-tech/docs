# Totem’s Winning Team

## Extended Technical Team

### Aeon C

**_International Accountant & Tax Consultant. (Singapore)_**

An experienced Chartered Accountant with a focus on accounting and taxation for digital economy. Passionate about blockchain technology and its real-world application. Has been actively contributing to various blockchain projects since 2017.

---

### Félix Daudré-Vigner 

**_Rust Developer (France)_**

Félix has recently joined the team and hit the ground running. A senior programmer with expertise in several languages, he moved into Rust development. He is charged with the process of migrating the code-base in preparation for Totem joining the Polkadot Network and beyond.

---

### Serge Van Ginderachter

**_Devops (Belgium)_**

Serge has worked for many large organisations managing their devops facilities. He joined Totem to manage infrastructure.



---