## Use of Funds 

<center >
<img src="/_media/use-of-funds.svg" alt="Use of funds chart" width="50%" height="auto">
<figcaption>
<i>Use of Funds Chart</i>
</figcaption>
</center>

Totem Developers will apply for grants from the Project Team Treasury for funding the following activities.

| Spend |Esitmated Percentage usage|
|----------------------------------------------|:------------------------:|
| Development & Engineering                    | 38.67%                   |
| Marketing                                    | 29.43%                   |
| Network & Operations                         | 15.29%                   |
| Pre-Crowdfunding Costs                       | 15.36%                   |
| Travel                                       | 1.24%                    |

Ideally the Totem Team would like to grow quickly. There may be further rounds of contribution.


