# Totem’s Winning Team

## Operations

### Lucas Katz

**_Head of Marketing and Communications (Canada)_**

Recently moved into the crypto space Lucas started his own podcast "Lets talk Nodesense" before joining the Totem Team. He currently works as a paramedic in Canada.

---

### Aruanan Mastenbroek

**_Business Development & Loyalty Program (Belgium)_**

Aruanan is a former commodities trader who moved into Crypto trading in 2018. He joined Totem to develop and manage a loyalty program wich will be launched sometime in 2024 as part of the low-barrier sign-up stage.

---

### Cheyenne Krishan

**_Consultant Press Relations (Belgium)_**

---