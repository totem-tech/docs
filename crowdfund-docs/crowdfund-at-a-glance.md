# Crowdfunding At A Glance.

---
**Presale inquiries welcome.** Send us an <a href="mailto:info@totemaccounting.com?subject=Presale Inquiry">email</a>.

* The Crowdfunding will take place over 5 weeks from a date to be announced before the end of June 2021.

* Early registrations should be made in the app: Visit [https://totem.live](https://totem.live/?form=NewsletterSignup). Formal registration will be released shortly. <!-- Visit [https://totem.live](https://totem.live/?module=Crowdfunding) -->

* The app will host both the Crowdfunding and aftersale decentralised OTC Marketplace.

* Minimum contribution is $100 paid with BTC, ETH, & DOT. 

**Soft Cap** 

* Raise $1,000,000  
* Allocated Quantity 2,149,834,178,385 TOTEM | 21,498 bTOTEM 
* Soft Cap Percentatge of Total Crowdfunding Supply TOTEM 21.50%
* Value of Allocation to Contributor: $ 100,000,000

**Target Cap (above Soft Cap)**

* Raise upto a further $2,500,000  
* Allocated Quantity 2,687,292,722,981 TOTEM |  26,873 bTOTEM
* Target Cap Percentatge of Total Crowdfunding Supply 26.87%
* Value of Allocation to Contributor: $ 125,000,000

**A regulatory assessment has been carried out and can be found in the [Regulation](Crowdfunding-docs/regulation.md) section.**

> Read further or just jump in to the [How do I Participate?](Crowdfunding-docs/Crowdfunding-how-to.md) section.