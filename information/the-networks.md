

## Totem Blockchain Network Information

### This page outlines the various Blockchain Networks that exist in the Totem ecosystem.

---

### Totem Meccano CanaryNet
#### [Testnet] 

This was the first Totem Network which launched in April 2019. It is still running and has been used to develop the Totem Blockchain and Front-end code. It is however, very old now, as it was based on the very early version on Polkadot Substrate V1 - the framework used to build Polkadot itself.

It is no-longer compatible with the current build status of Polkadot, but is still used by the team as a development testbed for new ideas. It did not need to be decentralised and was managed by the Totem Team. 

The tokens only have a notional value and are not intended to be listed on exchanges.

**Totem Meccano Canary Network will be deprecated when the KAPEX Parachain goes live.**

---

### Totem Lego (Wapex) Network
#### [Testnet Parachain] 

This network replaces Totem Meccano Canary Network and has been upgraded to the latest version of Polkadot Substrate V4.

It is connected as a parachain to Polkadot's Westend Test network although it is still coinsidered a test network used mainly by Totem developers. The team will still retain control of upgrades until the KAPEX Parachain is fully community owned.

The tokens only have a notional value and are not intended to be listed on exchanges and transactions are paid for with WND the currency of the Westend Network.

---

### Totem KAPEX Parachain Network
#### [LiveNet Parachain] 

This is Totem's flagship network being a Parachain on the Polkadot Network. It is expected to evolve over time to become community owned, and is the pre-MainNet chain. 

**The KAPEX Parachain Network will be Totem's Crowdloan candidate network and therefore will be heavily promoted.**

Users from Totem Meccano will have their balances transferred from Meccano to KAPEX provided they meet the conditions and it will be fully decentralised and community-owned once the Sudo super-user is removed.

There will be a very limited number of tokens issued (between 2-3M Tokens) on this network and can be obtained by community member via the Crowdloan, and from development grants.

**The will be no private sale, ICO or other VC distributions on this network.**

---

### Totem Live Accounting Blockchain Network
#### [MainNet] 

This is still some way off, and depending on the success of KAPEX, it may just evolve out of that project or become it's own chain. The decisions around this largely depend on how the market treats KAPEX. 

The view at this stage is that KAPEX coins will be burned to capitalise the Totem MainNet in a process with will create the TOTEM token on MainNet. This is outlined elsewhere

Stay tuned!

**In the next section we provide information about the TOTEM MainNet token.**