
# Contribution Terms.

## General Terms and Conditions of the Totem Live Accounting Crowdloan & Pledge.


**These terms have been updated on 15th July 2022 prior to the Pledge Round start date to include specific qualifying terms for the Pledge Round that will disqualify users who contribute directly to the Pledge Account and who have not contributed to the Crowdloan.**

---

**IMPORTANT NOTICE:** _Whilst we have made a concerted effort to explain the Totem Live Accounting Crowdloan & Pledge in great detail, you should not make any contribution unless you understand and are prepared to accept the risk. If you have any doubt about participating in the Totem Live Accounting Crowdloan & Pledge you should abstain from participating. You acknowledge that by contributing to the Totem Live Accounting Crowdloan & Pledge you are entering on your own accord having carried out your own research. The Live Accounting Association (Swiss Verein in formation), its members, partners and its associates, and the Totem Accounting Team, and consultants are not responsible for any loss that you incur from your investment or subsequent trades._

---

### About Your Contributions. 

The contributions are received and administered by the Live Accounting Association (Swiss Verein in formation). This organisation also administers the development grants in order to further the development of the Live Accounting Protocols and applications.

* **DOT Contributions to the Crowdloan are refundable** after the Parachain Lease period or end of the Crowdloan if the Auction does not secure a Parachain slot. 

> The Crowdloan has now closed - we won a slot!

* **DOT Contributions to the Pledge are not refundable once the Pledge Module is activated.**

### Contributing to the Pledge Round

#### Eligibility

1. Only Crowdloan contributors identified by an address that contributed to the Totem Kapex Crowdloan are eligible for participation in the Pledge Round with a few exceptions.

    1.a Any centralised exchange or DApp users are NOT eligible to participate due to technical reasons.

    1.b The only exception to this rule is contributors who participated in the Crowdloan via Parallel Finance are eligible to participate.

2. Any Crowdloan contribution made via a self-custody wallet such as PolkadotJS Apps, Nova, Feather or any other wallet are eligible. 

    * However the only way to participate is via our Dapp and the PolkadotJS Extension. Therefore the address that has been used to contribute to the Crowdloan **_must also be added to and available in the PolkadotJS Extension_**.

#### Pledge Rules

1. Pledged DOT are non-refundable (unlike Crowdloaned DOT).

2. You can Pledge and fulfill any amount starting from 0.01 DOT up to 100% of your Crowdloan contribution amount.

3. Pledge is not mandatory. Even if you didn't Pledge (or don't fulfill your Pledge) you will still receive your Crowdloan rewards.

4. If you choose not to fulfill your Pledge, you'll only receive Crowdloan portion of the rewards plus any referral bonuses and the additional share of the Guaranteed Allocations on 22% of the supply.

5. We recommend only using our DApp (https://totem.live/crowdloan) from a computer browser such as Chrome, Brave, Firefox with the Polkadotjs Extension installed.

6. If you have an eligible address sending funds manually to Totem's Pledge identity will still get you your Pledge rewards. 

    * However fulfilling this way may cause delays and you will miss out on referral rewards. 

    * For this reason we do not recommend sending funds directly to the Totem Pledge Address.

7. If you send more DOT than you are eligible the additional amount is non-refundable and will be considered a donation to the Live Accounting Association (Swiss Verein in formation). 

    * If you only use the DApp, it will prevent you from sending more than 100%.


#### Totem Pledge Identity 

You can view the official Totem Accounting Pledge Identity here:

https://polkadot.subscan.io/account/14K5BeQDAwETVu9c7uRnxixW1DRefrbawD8yima2Mv2nR651

#### Other terms

* Contributors are required to self-validate the amounts that they fulfill for the Pledge Round, and that the addresses that they are making the Pledge fulfillment from is the correct address:

* Any address that sends funds to the Pledge Account directly, but did not contribute to the Crowdloan, will not receive KAPEX allocations.

    * It is a mandatory requirement of the Pledge Round that you must already have contributed to the Crowdloan. There are no exceptions i.e. no secret private deals - everything is public.

    * The Pledge fulfillment address that you use to send funds **_MUST CORRESPOND WITH THE CROWDLOAN CONTRIBUTION ADDRESS_** to qualify for allocations of KAPEX. There are no exceptions.

    * If you did not contribute to the Crowdloan, you can still send DOT funds to the Pledge Account on Polkadot directly **but this will be considered an unconditional and unrewarded donation to the team.**

>We strongly recommend that you use this application to avoid errors. You can access it at [totem.live/crowdloan](https://totem.live/crowdloan)

* The same address used for the crowdloan and / or pledge will be used to payout all Kapex Allocations and bonus rewards.

* A Contributor can Pledge **at any time before the Pledge period ends**, and provided they have followed the terms set out here.

> The Pledge Round starts at midday CET on 18th July 2022 and will end on the 8 August 2022 at midnight CET.

* All Pledges received after the Pledge period ends will not receive KAPEX allocations. This cannot be appealed.

* The final KAPEX allocation is only calculated after the Pledge period ends in order to capture Pledges made at the very last minute. This will be a manual process but it will be published.

* When the KAPEX allocations are released and allocated, the funds will be available for transfer once the balance transfer lock has been disabled. Refer to the [Crowdloan FAQ](http://localhost:3000/#/crowdloan/crowdloan-faq?id=_8-when-will-my-total-allocations-be-calculated). 

* All bonus payments are set out in the [Crowdloan Details](/crowdloan/crowdloan-details.md).

### TOTEM Meccano Testnet Account Migration & Allocations

Funds that are migrated from Totem Meccano will be carried out on a fair basis. If we notice any suspicious activity on Totem Meccano for example this includes but is not limited to using fake email addresses in the newsletter signup, fake or bot Twitter accounts, gaming the distribution mechanism or adapting or resetting the Testnet client UI your funds will not be migrated and you will be disqualified. You may raise a complaint on our social channels (Twitter, Discord or Telegram) if you believe that you have been unfairly blocked from receiving a migrated balance. Final decisions will be at the discretion of the Project Team.


Participants will be expected to carry out several on-chain and off chain tasks in order to claim their reward on KAPEX Parachain and a special module has been set up in the [Totem Live App](https://totem.live).

The lifting of the balance transfer lock on Kapex is not dependent on the migration allocations having been completed. You are therefore encouraged to carry out the migration tasks at the earliest opportunity. Once the migration process deadline passes, Totem Meccano will be deprecated, and users will no longer be able to migrate their balances. There will be no exceptions to this rule.

#### Terms of Migration

The terms of the migration are as follows:

> The Migration of these funds were announced 14th November 2022. The claim process will last until midnight CET 31st January 2023. 

* 1) The original faucet allocations were made free-of-charge and were to be allocated to a single unique chat user, and unique identity combination. 

        * This rule has not changed and applies in the claim process.

* 2) Referral allocations were made on a _1:n_ per unique chat user unique identity combination. 

        * This means that one unique combination can refer multiple other unique combinations as long as neither the referred chat user nor the identity had been previously registered or was otherwise in direct receipt of faucet funds at the time of referral.


* 3) In the case of the inclusion of a Twitter handle for claiming airdrop rewards, only one Twitter handle is allowed per identity. All subsequent uses of any given Twitter handle will be disqualified from receiving migrated allocations of Kapex coins.  

* 4) Only one allocation will be considered claimable per unique combination. 

        * Using multiple user accounts with the same identity is considered gaming the reward mechanism and will result in disqualification for all claims for that user and or identity. 


* 5) In the case of referrals only "the first seen" referral will count. All subsequent referral attempts for either a chat user or identity will not be counted for migration.